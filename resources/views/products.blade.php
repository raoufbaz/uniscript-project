@inject('Comment', 'App\Comment')
@inject('Rating', 'App\Rating')
@inject('Sale', 'App\Sale')
@extends('Layout')
@section('content')
<div class="container-fluid p-0">
	<div class="row bg-white m-0 p-2">
		<div class="col-lg-3 col-md-12 col-sm-12 col-12">
			<h2 class="grid-title text-primary"><i class="fa fa-filter"></i> @lang('home.Filters')</h2>
			<hr>
			<h4 class="p-0 mt-5">@lang('home.By category'):</h4>
            <select class="filtercats">
            </select>
                <button class="btn btn-outline-success my-5 w-100 float-right" onclick="changeCat();">@lang('home.Apply') <i class="fa fa-check"></i></button>
            <br>
             <h4 class="p-0 mt-5">@lang('home.By rating'):</h4>
            <div class="row btn btn-outline-light text-dark border-0 w-100" data-rating="4" onclick="changeRating(4);">
                <div class="col-12 p-0">
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    & @lang('home.Up')
                </div>
            </div>
            <div class="row btn btn-outline-light text-dark border-0 w-100" data-rating="3" onclick="changeRating(3);">
                <div class="col-12 p-0">
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    &  @lang('home.Up')
                </div>
            </div>
            <div class="row btn btn-outline-light text-dark border-0 w-100" data-rating="2" onclick="changeRating(2);">
                <div class="col-12 p-0">
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    & @lang('home.Up')
                </div>
            </div>
            <div class="row btn btn-outline-light text-dark border-0 w-100" data-rating="1" onclick="changeRating(1);">
                <div class="col-12 p-0">
                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    <i class="fa fa-star text-muted" aria-hidden="true"></i>
                    & @lang('home.Up')
                </div>
            </div>
			<h4 class="p-0 mt-5">@lang('home.By price'):</h4>
 			<h3 class="p-2 m-0 pricetitle">@lang('home.Less than'): <b><i class="priceselected">200</i>$</b></h3>
           <div class="slidecontainer">
              <input type="range" min="5" max="2000" value="200" class="sliderprice" id="myRange">
            </div>
            <button class="btn btn-outline-success my-5 p-2 float-right w-100" onclick="changePrice();">@lang('home.Apply')<i class="fa fa-check"></i></button>
		  </div>
		  <div class="col-lg-9 col-md-12 col-sm-12 col-12">
			<h2 class="text-warning"><i class="fa fa-file-o"></i> @lang('home.Result')</h2>
			
			<hr>				
			<div class="row">
			  <div class="col-sm-6">
				<div class="input-group-btn">
                    <button type="button" class="btn text-danger dropdown-toggle" data-toggle="dropdown">
						<span id="order_concept">@lang('home.Order by')</span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
                        <li><div class='nav-link btn-outline-dark orderclick' data-order="no-order">@lang('home.None')</div></li>
                        <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-name">@lang('home.Name')</div></li>
                        <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-price-desc">@lang('home.Price : high to low')</div></li>
                        <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-price-asc">@lang('home.Price : low to high')</div></li>
                        <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-rating-desc">@lang('home.Rating : high to low')</div></li>
                    	<li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-rating-asc">@lang('home.Rating : low to high')</div></li>
                    </ul>
				</div>
			  </div>					  
			  <div class="col-md-6 text-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-white btn-outline-danger active listbtn" onclick="list();"><i class="fa fa-list"></i></button>
				  <button type="button" class="btn btn-white btn-outline-danger gridbtn" onclick="grid();"><i class="fa fa-th"></i></button>
				</div>
			  </div>
            </div>
            
			<div class="table-responsive cart-table">
			  <table class="table table-hover table-Products list-mode">
				<tbody>
			  @forelse ($Products as &$prod) 	
				<tr>
				  <td onclick="redirect({{$prod->id}})" class="image text-center"><img src="{{asset('images/Products/'.$prod->id.'.jpg')}}" alt=""></td>
                  <td onclick="redirect({{$prod->id}})" class="Product">
                    <script>
                        	var lang="{{ Session::get('locale') }}";
						if(lang=="fr"){

					document.write("<strong>{{$prod->namefr}}</strong><br>{{$prod->descfr}}</td>");
					}
					else if(lang=="en"){
						document.write("<strong>{{$prod->name}}</strong><br>{{$prod->desc}}</td>");}

						else{document.write("<strong>{{$prod->name}}</strong><br>{{$prod->desc}}</td>");}
                   
</script>
				  <td onclick="redirect({{$prod->id}})" class="rate text-right text-primary"><span>
                  @if($Rating->where('idprod','=', $prod->id)->avg('stars') != null)
                      @for ($x = 0; $x < round($Rating->where('idprod','=', $prod->id)->avg('stars')); $x++) 
                             <i class="fa fa-star text-warning"></i>
                      @endfor
                      @for ($x = 0; $x < (5-round($Rating->where('idprod','=', $prod->id)->avg('stars'))); $x++)
                            <i class="fa fa-star text-muted"></i>
                      @endfor
                  @else
                       <span></span>
                  @endif
                  </span></td>
				  <td class="price text-center row text-success m-0">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 mx-1 p-0">{{$prod->price}}$</div>
                    @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
                    <button onclick="addToCart('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-lg-12 col-md-12 col-sm-5 col-5 btn btn-white text-success border-0 add-to-cart rounded"><i class="fa fa-2x fa-cart-plus"></i></button>	
                    <button onclick="addToWishlist('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-lg-12 col-md-12 col-sm-5 col-5 btn btn-white border-0 add-to-wishlist rounded"><i class="fa fa-2x fa-star star-wishlist" style=" " aria-hidden="true"></i></i></button>	
                    @endif
                  </td>
				</tr>
              @empty
                    <p class="text-muted display-3 text-center p-4"> @lang('home.found')</p>
              @endforelse
			  </tbody>
              </table>
              <div class="row p-0 m-0 grid-mode d-none">
              @foreach ($Products as &$prod)
				<div class="col-lg-4 col-md-6 col-sm-6 col-12 p-3" style="float:left !important;">
	                <figure class="card card-Product shadow">
                        <div  class="badge p-0 text-white bg-warning" style="position:absolute;top:0;right:0;z-index:1;font-size:1.3em;"><i class="fa fa-star p-1"> {{$Rating->where('idprod','=', $prod->id)->avg('stars')!=null?number_format((float)$Rating->where('idprod','=', $prod->id)->avg('stars'), 1, '.', ''):'no-rating'}}</i></div>
                        <div  class="badge p-0 text-white bg-success" style="position:absolute;top:0;left:0;z-index:1;font-size:1.3em;"><i class="fas fa-dollar-sign p-1"> {{$prod->price}}</i></div>
		                <div class="img-wrap"  onclick="redirect({{$prod->id}})"><img src="{{asset('images/Products/'.$prod->id.'.jpg')}}"></div>
		                <figcaption class="info-wrap"  onclick="redirect({{$prod->id}})">
				                <h4 class="title"><script>
                                		var lang="{{ Session::get('locale') }}";
						if(lang=="fr"){
                            document.write(("{{preg_replace( "/\r|\n/", "", $prod->namefr )}}").substr(0,19) + "...");

					
					}
					else if(lang=="en"){
		
                        document.write(("{{preg_replace( "/\r|\n/", "", $prod->name )}}").substr(0,19) + "...");
						
					}
                    else{ document.write(("{{preg_replace( "/\r|\n/", "", $prod->name )}}").substr(0,19) + "...");}
                               </script></h4>
				                <p class="desc"><script>
                                                                    		var lang="{{ Session::get('locale') }}";
						if(lang=="fr"){
                            document.write(("{{preg_replace( "/\r|\n/", "", $prod->descfr )}}").substr(0,40) + "...");

					
					}
					else if(lang=="en"){
                        document.write(("{{preg_replace( "/\r|\n/", "", $prod->desc )}}").substr(0,40) + "...");
         
						
					}
                    else{  document.write(("{{preg_replace( "/\r|\n/", "", $prod->desc )}}").substr(0,40) + "...");}

                                </script></p>
				                <div class="rating-wrap">
					                <i class="label-rating">{{$Comment->where('idprod','=', $prod->id)->count()}} @lang('home.review(s)')</i>
					                <i class="label-rating">{{$Sale->where('idprod','=', $prod->id)->count()}} @lang('home.order(s)') </i>
				                </div> 
		                </figcaption>
			            @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
		                <div class="bottom-wrap row m-0">
                            <button onclick="addToCart('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-6 btn btn-white text-success float-left border-0 add-to-cart rounded"><i class="fa fa-2x fa-cart-plus"></i></button>	
                            <button onclick="addToWishlist('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-6 btn btn-white float-right border-0 add-to-wishlist rounded"><i class="fa fa-2x fa-star star-wishlist" style=" " aria-hidden="true"></i></i></button>	
                        </div>
                        @endif
	                </figure>
                </div>
                @endforeach
			  </div>
			</div>
			{{ $Products->onEachSide(1)->links() }}

		</div>
	</div>
</div>

<script>
    $( ".pagination" ).addClass( "justify-content-center" );
    var pathArray = (window.location.pathname).substring(window.location.pathname.indexOf("Products/")).split('/');
    var catid = pathArray[1];
    var orderby = pathArray[2];
    var price = pathArray[3];
    var rating = pathArray[4];
    var searchterm="";
    if(pathArray[5])
    {
        var searchterm = pathArray[5];
        searchterm = decodeURI(searchterm);
    }
    $('.searchbox').val(searchterm);
    if(orderby!="no-order")
    {
        $('#order_concept').html("@lang('home.Ordered by') "+$("div").find("[data-order='"+orderby+"']").html());
    }
    if(price!="no-price")
    {
        $('.sliderprice').val(price);
        $('.priceselected').html(price);
    }
    if(rating!="no-rating")
    {
       $("div").find("[data-rating='"+rating+"']").addClass('active');
    }
    var ProductsViewMode = getCookie("ProductsViewMode");
    if (ProductsViewMode!="" && ProductsViewMode=="grid")
	{
		grid();
	}
    else
    {
		list();
	}
	$(document).ready(function(){
             cat="<option value='0'>@lang('home.all')</option>";
             $( ".filtercats" ).append( cat );
             $.ajax({
                type: "get",
                async:true,
		        url: "{{ url('Layoutcat') }}",
		        success: function(data){
			        data.Categories.forEach(function(item){
                        var lang="{{ Session::get('locale') }}";
					
                    if(lang=="fr"){
                        cat="<option value='"+item.id+"'>"+item.namefr+"</option>";
		     	        $( ".filtercats" ).append( cat );
               }
                    else if(lang=="en"){
                        cat="<option value='"+item.id+"'>"+item.name+"</option>";
		     	        $( ".filtercats" ).append( cat );

                    }
                    else{
                        cat="<option value='"+item.id+"'>"+item.name+"</option>";
		     	        $( ".filtercats" ).append( cat );
                    }
                    
			        });
                    },
                    complete: function (data) {
                       $(".filtercats").val(catid);
                       $('.search-panel span#search_concept').text($(".filtercats").find(":selected").text());
                       $('.cats').attr("data-idcat",catid);
                     }
             });
            
             $(document).on("click", '.orderclick', function(event) {
                    var order = $(this).attr('data-order');
					url = '{{ url("Products/id/order/price/rating/name") }}';
					url = url.replace('id', catid);
					url = url.replace('name', searchterm);
					url = url.replace('price', price);
					url = url.replace('rating', rating);
                    url = url.replace('order', order);
					window.location.href = url;
             });
            
});
function changePrice(){
    var price = $('.sliderprice').val();
	url = '{{ url("Products/id/order/price/rating/name") }}';
                url = url.replace('id', catid);
				url = url.replace('name', searchterm);
				url = url.replace('price', price);
                url = url.replace('rating', rating);
                url = url.replace('order', orderby);
				window.location.href = url;
}
function changeRating(stars){
	url = '{{ url("Products/id/order/price/rating/name") }}';
                url = url.replace('id', catid);
				url = url.replace('name', searchterm);
				url = url.replace('price', price);
                url = url.replace('rating', stars);
				url = url.replace('order', orderby);
				window.location.href = url;
}
function changeCat(){
	var id =$(".filtercats").find(":selected").val();
	url = '{{ url("Products/id/order/price/rating/name") }}';
                url = url.replace('id', id);
				url = url.replace('name', searchterm);
				url = url.replace('price', price);
				url = url.replace('rating', rating);
                url = url.replace('order', orderby);
				window.location.href = url;
}

$('#myRange').on('input', function() {
  text = $('#myRange').val();
  $('.priceselected').html(text);
});
function redirect(id) {
	             url = '{{ url("Product_infos/id") }}';
                url = url.replace('id', id);
				window.location.href = url;
	
}
function list() {
	$('.list-mode').removeClass("d-none");
	$('.grid-mode').addClass("d-none");
    $('.listbtn').addClass("active");
    $('.gridbtn').removeClass("active");
    setCookie("ProductsViewMode","list","5");
}
function grid() {
	$('.list-mode').addClass("d-none");
	$('.grid-mode').removeClass("d-none");
    $('.gridbtn').addClass("active");
    $('.listbtn').removeClass("active");
    setCookie("ProductsViewMode","grid","5");
}
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	var c = ca[i];
	while (c.charAt(0) == ' ') {
		c = c.substring(1);
	}
	if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	}
	}
	return "";
}
</script>
@endsection
