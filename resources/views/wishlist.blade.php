@extends('Layout')

@section('content')
@if(Session::has('Wishlist'))
<div class="pb-5">
    <div class="container">
      <div class="row rounded shadow-sm mb-5">
        <div class="col-lg-12 p-2 bg-white ">
           <div onclick="removeAll()" class="m-auto btn btn-white text-danger border-0 rounded" style="font-size:1.3em;cursor:pointer;"><i class="fa fa-times pull-left"> @lang('home.Clear Wishlist')</i></div>
        </div>
        <div class="col-lg-12 p-5 bg-white ">
          <div class="table-responsive cart-table">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col" class="border-0 bg-light">
                    <div class="p-2 px-3 text-uppercase">@lang('home.Product')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Price')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Remove')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Add')</div>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($Products as $Product)
                <tr>
                  <th scope="row" >
                    <div class="p-2 prod-img">
                      <img src="{{asset('images/Products/'.$Product['item']['id'].'.jpg')}}" alt="" width="70" class="img-fluid rounded shadow-sm">
                      <div class="ml-3 d-inline-block align-middle">

                        <h5 class="mb-0"> 
                          @if(Session::get('locale')=="fr")
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['namefr'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@lang('home.Category'): {{$Product['category']}}</span>
                          
                          @elseif(Session::get('locale')=="en")
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['name'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@lang('home.Category'): {{$Product['category']}}</span>
                          
                          @else
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['name'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@lang('home.Category'): {{$Product['category']}}</span>
                          @endif
                      </div>
                    </div>
                  </th>
                  <td class="align-middle"><strong class="text-success">${{$Product['price']}}</strong></td>
                  <td class="align-middle"><button onclick="remove('{{$Product['item']['id']}}')" class="btn btn-white text-danger"><i class="fa fa-trash" style="font-size:1.4em;"></i></button></td>
                  <td class="align-middle"><button onclick="addToCartAndDeleteFromWishlist('{{$Product['item']['id']}}');" data-idprod="{{$Product['item']['id']}}" class="btn btn-white text-success border-0 add-to-cart rounded"><i class="fa fa-2x fa-cart-plus"></i></button></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>


    </div>
  </div>
    @else
    <div class="text-muted text-center py-5 m-auto" style="word-wrap: break-word;font-size:1.3em"><big class="p-0 m-0">@lang('home.Your wishlist is') <b>@lang('home.empty')</b>.</big><br><br><i>@lang('home.Come back later')!</i></div>
    @endif
    <script>
    function remove(id)
    {
       var r=confirm("@lang('home.Are you sure')!");
       if(r==true)
       {
           var url="{{route('Product.removeFromWishlist',['id' =>':id'])}}";
           url = url.replace(':id', id);
		   window.location.href = url;
        }
    }
    function removeAll()
    {
       var r=confirm("@lang('home.Are you sure')!");
       if(r==true)
       {
           var url="{{route('Products.removeAllFromWishlist')}}";
		   window.location.href = url;
        }
    }

    function addToCartAndDeleteFromWishlist(id)
    {
        var r=confirm("Are you sure!");
       if(r==true)
       {
            var url="{{route('Product.addToCart',['id' =>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
            type: "get",
            async:true,
		    url: url,
		    success: function(data){
            $('.Wishlist-items').text(data.items);
                   $('#snackbar').html(data.snackbar);
                  notify();
			    },
            complete: function(data){
               var url="{{route('Product.removeFromWishlist',['id' =>':id'])}}";
               url = url.replace(':id', id);
		       window.location.href = url;
            }
            });
        }

    }

    </script>
@endsection
