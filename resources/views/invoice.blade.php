@extends('Layout')

@section('content')


<div class="container">
  <h2>Dashbord</h2>
  <div class="col-4">
    <button  class="btn btn-success "  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Add</button>
  </div>
  <div div class="collapse" id="collapseExample">
    <h3 style="margin-left:40%;margin-bottom:5%"><u>New Invoice</u></h3>
    <form id="add" class="form" method="post" action="{{action('PublicController@addinvoice')}}">
      @csrf
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>User</label>
                <select class="form-control" name="user" >
                  @foreach ($users as $c)
                  <option value="{{$c->id}}">{{$c->email}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label>Product</label>
              <select class="form-control" name="prod" >
                @foreach ($prods as $c)
                <option value="{{$c->id}}">{{substr($c->name,0,40)}}...</option>
                @endforeach
              </select>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Quantity</label>
                <input required class="form-control lastname" type="text" name="qty" placeholder="1" value="">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Discount</label>
                <input required class="form-control firstname" type="decimal" name="disc" placeholder="0.00" value="">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>SubTotal</label>
                <input required class="form-control firstname" type="decimal" name="sub" placeholder="0.00" value="">
              </div>
            </div>
          </div>
          <div class="row">
               <div class="col d-flex justify-content-start">
                   <button  class="btn btn-primary" type="submit">Create bill</button>
               </div>
          </div>
          @if(Session::has('addres'))
            <p class="text-success">
                 {{ Session::get('addres') }}
            </p>
          @endif
      </form>
      
   </div>

  <div class="input-group col-lg-4 col-md-11 col-sm-11 col-11 m-auto p-0  d-flex justify-content-between">
   
    <input type="text" autocomplete="off" class="form-control search" id="search" name="search" placeholder="Search..."  >
    <span class="input-group-btn">
      <button class="btn btn-outline-info searchb" type="button"><i class="fas fa-search p-1"></i></button>
    </span>
                  <br/>
                  <table class="table table-hover bg-white" style="position: absolute;top: 100%;width: 100%;z-index:3" id="livesearch"></table>
  </div>                                                                                   
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Date</th>
        <th>Total</th>
        <th> Check Bill </th>
    </tr>
    </thead>
    <tbody>
      @foreach ($Sales as $s)
      <tr>
        <td>{{$s->iduser}}</td>
        <td>{{$s->firstname ." " .$s->lastname}} </td>
        <td>{{$s->saledate}}</td>
        <td>{{number_format((float)$s->total*1.15-$s->discount, 2, '.', '')." $"}}</td>
        <td><a href="{{route('bill_details', ['iduser'=>$s->iduser, 'saledate'=>$s->saledate, 'total'=>$s->total]) }}" class="nav-link text-dark font-italic">
          <i class="fas fa-eye text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
       </tr>
      @endforeach
    </tbody>
  </table>
  {{ $Sales->onEachSide(1)->links() }}
  </div>
</div>


<script>

 $(".searchb").click(function(){
  
  
  url="{{url('invoice/id')}}";
  url = url.replace('id',  $('#search').val());
  window.location.href = url;
});

</script>

  
  @endsection
