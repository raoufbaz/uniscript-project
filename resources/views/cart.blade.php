@extends('Layout')

@section('content')
@if(Session::has('cart'))
<div class="pb-5">
    <div class="container">
      <div class="row rounded shadow-sm mb-5">
        <div class="col-lg-12 p-2 bg-white ">
           <div onclick="removeAll()" class="m-auto btn btn-white text-danger border-0 rounded" style="font-size:1.3em;cursor:pointer;"><i class="fa fa-times float-left">@lang('home.Clear Cart')</i></div>
        </div>
        <div class="col-lg-12 p-5 bg-white ">
          <div class="table-responsive cart-table">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col" class="border-0 bg-light">
                    <div class="p-2 px-3 text-uppercase">@lang('home.Product')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Price')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Quantity')</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">@lang('home.Remove')</div>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($Products as $Product)
                <tr>
                  <th scope="row" >
                    <div class="p-2 prod-img">
                      <img src="{{asset('images/Products/'.$Product['item']['id'].'.jpg')}}" alt="" width="70" class="img-fluid rounded shadow-sm">
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"> 
                          @if(Session::get('locale')=="fr")
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['namefr'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@LANG('home.Category'): {{$Product['category']}}</span>
                          
                          @elseif(Session::get('locale')=="en")
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['name'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@LANG('home.Category'): {{$Product['category']}}</span>
                          
                          @else
                          <a href="{{ url('Product_infos/'.$Product['item']['id'].'') }}" class="text-dark d-inline-block align-middle">{{substr($Product['item']['name'],0,20)}}...</a></h5><span class="text-muted font-weight-normal font-italic d-block">@LANG('home.Category'): {{$Product['category']}}</span>
                          @endif
                      </div>
                    </div>
                  </th>
                  <td class="align-middle"><strong class="text-success">${{$Product['item']['price']}}</strong></td>
                  <td class="align-middle">
                    <div class="number-input md-number-input row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 p-2">
                            <input id="quantity-{{$Product['item']['id']}}" min="1" name="quantity" value="{{$Product['qty']}}" type="number" disabled>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 p-2">
                            <i onclick="addOne('{{$Product['item']['id']}}')" class="btn btn-success text-white fas fa-plus plus-one"></i>
                            <i onclick="removeOne('{{$Product['item']['id']}}')" class="btn btn-danger text-white fas fa-minus minus-one-{{$Product['item']['id']}}"></i>
                        </div>
                    </div>
                  </td>
                  <td class="align-middle"><button onclick="remove('{{$Product['item']['id']}}')" class="btn btn-white text-danger"><i class="fa fa-trash" style="font-size:1.4em;"></i></button></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="row py-5 p-4 bg-white rounded shadow-sm">
        <div class="col-lg-6">
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Coupon code</div>
          <div class="p-2">
            <p class="font-italic mb-4">@lang('home.If you have a coupon code, please enter it in the box below')</p>
            <form onsubmit="return false;" class="input-group row p-0 mb-4 border rounded-pill p-2">
              <div class="col-lg-1 col-md-1 col-sm-1 col-1 py-1"><span class="text-muted p-0 code-returned"></span></div>
              <div class="col-lg-7 col-md-7 col-sm-10 col-10"><input type="text" placeholder="@lang('home.Apply coupon')" class="form-control border-0 coupon-code" required></div>
              <div class="input-group-append border-0 col-lg-4 col-md-4 col-sm-12 col-12  p-0">
                <button type="button" onclick="applyCoupon()" class="btn btn-dark float-right w-100 rounded-pill coupon-btn" style="z-index:1;"><i class="fa fa-gift mr-2"></i>@lang('home.Apply coupon')
                </button>
              </div>
            </form>
            
          </div>
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">@lang('home.Instructions for our employees')</div>
          <div class="p-4">
            <p class="font-italic mb-4">@lang('home.If you have some information for the employees you can leave them in the box below')</p>
            <textarea name="" cols="30" rows="2" class="form-control instructions"></textarea>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">@lang('home.Order summary') </div>
          <div class="p-4">
            <p class="font-italic mb-4">@lang('home.Coupons and additional discounts are shown below.You can only apply one at a time').</p>
            <ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">@lang('home.Order Subtotal') </strong><strong class="text-dark"><span class="subtotal-price">{{sprintf('%0.2f',$totalPrice)}}</span>$</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">@lang('home.Coupons & discounts')</strong><strong class="text-danger">-<span class="coupon-price">{{sprintf('%0.2f',$totalPrice*($coupon/100))}}</span>$</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">@lang('home.tax')</strong><strong  class="text-muted tax-price">+{{sprintf('%0.2f', $totalPrice*0.15)}}$</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Total</strong>
                <h5 class="font-weight-bold text-success total-price">{{sprintf('%0.2f', ($totalPrice*1.15)-($totalPrice*($coupon/100)))}}$</h5>
              </li>
            </ul>
            @if(Auth::guard('web')->check())
            <button class="btn btn-success rounded-pill py-2 btn-block reserve-btn" type="button" onclick="reserve()">@lang('home.Reserve in store')</button>
            @else
            <button class="btn btn-primary rounded-pill py-2 btn-block" type="button" onclick="openForm()">@lang('home.log')</button>
            <div class="form-popup" id="myForm">
                <form role="form" method="post" name="form" action="{{ route('login') }}" class="form-container">
                @csrf
                <h1>Login</h1>

                <label for="email"><b>Email</b></label>
                <input type="email" name="email" autocomplete="username" placeholder="@lang('home.user')" required>
                @error('email')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
                <label for="psw"><b>Password</b></label>
                <input type="password" name="password" autocomplete="current-password" placeholder="@lang('home.pass')" required>
                @error('password')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
                <button type="submit" class="btn btn-success rounded-pill py-2 btn-block">@lang('home.log')</button>
                <button type="button" class="btn btn-danger rounded-pill py-2 btn-block" onclick="closeForm()">@lang('home.cancel')</button>
                </form>
            </div>
            @endif
          </div>
        </div>
      </div>

    </div>
  </div>
    @else
    <div class="text-muted text-center py-5 m-auto" style="word-wrap: break-word;font-size:1.3em"><big class="p-0 m-0">@lang('home.Your shopping cart is') <b>@lang('home.empty')</b>.</big><br><br><i>@lang('home.Come back later')!</i></div>
    @endif
    <script>
    function remove(id)
    {
       var r=confirm("@lang('home.Are you sure')!");
       if(r==true)
       {
           var url="{{route('Product.removeFromCart',['id' =>':id'])}}";
           url = url.replace(':id', id);
		   window.location.href = url;
        }
    }
    function removeAll()
    {
       var r=confirm("@lang('home.Are you sure')!");
       if(r==true)
       {
           var url="{{route('Products.removeAllFromCart')}}";
		   window.location.href = url;
        }
    }
    function addOne(id)
    {
        
      document.getElementById('quantity-'+id).stepUp();
      if($('#quantity-'+id).val()>1)
        {
            $('.minus-one-'+id).removeClass("over");
        }
      var url="{{route('Products.addOne',['id' =>':id'])}}";
        url = url.replace(':id', id);
        $.ajax({
            type : 'get',
            url : url,
            async:true,
            success:function(data){
            $('.subtotal-price').html(data.subtotal);
            $('.tax-price').html(data.tax);
            $('.coupon-price').html(data.coupon);
            $('.total-price').html(data.total);
            }
        });
    }
    function removeOne(id)
    {
        if($('#quantity-'+id).val()==1)
        {
            $('.minus-one-'+id).addClass("over");
        }
        else
        {
            document.getElementById('quantity-'+id).stepDown();
            var url="{{route('Products.removeOne',['id' =>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                type : 'get',
                url : url,
                async:true,
                success:function(data){
                $('.subtotal-price').html(data.subtotal);
                $('.tax-price').html(data.tax);
                $('.coupon-price').html(data.coupon);
                $('.total-price').html(data.total);
                }
            });
        }
        
    }
    function applyCoupon()
    {
        $coupon= $('.coupon-code').val();
        if($coupon!='')
        {
            var url="{{route('Products.addCoupon',['coupon' =>':coupon'])}}";
            url = url.replace(':coupon',$coupon);
            $.ajax({
                type : 'get',
                url : url,
                async:true,
                beforeSend: function(){
                    $('.code-returned').html('<i class="fas fa-2x fa-spinner fa-spin"></i>');
                },
                success:function(data){
                $('.code-returned').html(data.returned);
                $('.subtotal-price').html(data.sub);
                $('.tax-price').html(data.tax);
                $('.coupon-price').html(data.coupon);
                $('.total-price').html(data.total);
                }
            });
        }

    }
    function reserve()
    {
        var url="{{route('Products.reserve',['discount' =>':discount','instructions' =>':instructions'])}}";
        $discount=$(".coupon-price").text();
        $instructions=$(".instructions").val();
        
       if($instructions=='undefined'|| $instructions==''){
       $instructions="No Instruction";
       
       }
       
        url = url.replace(':instructions',$instructions);
        url = url.replace(':discount',$discount); 
        $.ajax({
                type : 'get',
                url : url,
                async:true,
                beforeSend: function(){
                    $('.reserve-btn').html('<i class="fas fa-2x fa-spinner fa-spin"></i>');
                },
                success:function(data){
                alert(data);
                location.reload();
                }
            });
    }
    function openForm() {
     document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
      document.getElementById("myForm").style.display = "none";
    }

    </script>
@endsection
