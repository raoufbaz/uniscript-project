@extends('Layout')

@section('content')

<div class="row pt-4">

	<div class="col">
		<div class="card">
			<div class="card-header bg-primary text-white"><i class="fa fa-envelope"></i> @lang('home.contact_p').
			</div>
			<div class="card-body">
				<form id='fo' action="{{action('PublicController@contact')}}" method=post>
					@csrf
					<div class="form-group">
						<label for="email">@lang('home.Email Address')</label>
						<input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="@lang('home.enemail')" required>
						<small id="emailHelp" class="form-text text-muted">@lang('home.reply')</small>
					</div>
					<div class="form-group">
						<label for="message">Message</label>
						<textarea class="form-control" id="message" name="message" rows="6" required></textarea>
					</div>
					<div class="mx-auto">
					<button type="submit" class="btn btn-primary text-right">@lang('home.submit')</button></div>

				</form>
			<p 
			>@if(Session::has('message'))
				{{ Session::get('message') }}

				<script>document.getElementById('fo').style.display="none";</script>

				@endif</p>

			</div>
		</div>
	</div>
	<div class="col-12 col-sm-4">
		<div class="card bg-light mb-3">
			<div class="card-header bg-success text-white text-uppercase"><i class="fa fa-home"></i> @lang('home.Address')</div>
			<div class="card-body">
				<p>6125 rue Jean-Talon est MONTRÉAL</p>
				<p>H1S1M6 QUEBEC</p>
				<p>CANADA</p>
				<p>Email : email@example.com</p>
				<p>@lang('home.Tel'): +14389896770</p>

			</div>

		</div>
	</div>
</div>

@endsection