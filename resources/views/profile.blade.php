@extends('Layout')

@section('content')



<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
<div class="row flex-lg-nowrap">
 
  @if(Auth::guard('web')->check())
  <div class="col">
    <div class="row">
      <div class="col mb-3">
        <div class="card">
          <div class="card-body">
            <div class="e-profile">
              <div class="row">
                <div class="col-12 col-sm-auto mb-3">
                  <div class="mx-auto" style="width: 140px;">
                    <img src="images/users/{{Auth::user()->id}}.jpg" class="img-fluid rounded-circle"alt="">
                  </div>
                </div>
                <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                  <div class="text-center text-sm-left mb-2 mb-sm-0">
                    <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">{{Auth::user()->firstname}}</h4>
                    <p class="mb-0">{{Auth::user()->lastname}}</p>
                    <div class="text-muted"><small>Last seen 2 hours ago</small></div>
                    <div class="mt-2">
                      <button class="btn btn-primary" type="button">
                        <i class="fa fa-fw fa-camera"></i>
                        <span>Change Photo</span>
                      </button>
                    </div>
                  </div>
                  <div class="text-center text-sm-right">
                    <span class="badge badge-secondary">{{Auth::user()->statut}}</span>
                    <div class="text-muted"><small>Joined 09 Dec 2017</small></div>
                  </div>
                </div>
              </div>
              <ul class="nav nav-tabs">
                <li class="nav-item"><a href="" class="active nav-link">Settings</a></li>
              </ul>
              <div class="tab-content pt-3">
                <div class="tab-pane active">
                  <form class="form" method="post" action="{{action('PublicController@edituserinfo')}}" >
                    @csrf
                    <div class="row">
                      <div class="col">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>First Name</label>
                              <input class="form-control" type="text" name="firstname" placeholder="{{Auth::user()->firstname}}" value="{{Auth::user()->firstname}}">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input class="form-control" type="text" name="lastname" placeholder="{{Auth::user()->lastname}}" value="{{Auth::user()->lastname}}">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Email</label>
                              <input   disabled class="form-control"    type="text" value="{{Auth::user()->email}}">
                              <input    class="form-control"  name="email"  type="hidden" value="{{Auth::user()->email}}">
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="row">
                      <div class="col d-flex justify-content-start">
                        <button  class="btn btn-primary" type="submit">Save Changes</button>
                      </div>
                    </div>
                  </form>
                  @if(Session::has('mens'))
                  <p class="text-success">
                                    {{ Session::get('mens') }}
                                   
                  </p>
                                    @endif
                    <br>
                    <form action="{{action('PublicController@updateuser')}}"  method="get">
                    <div class="row">
                      <div class="col-12 col-sm-6 mb-3">
                        <div class="mb-2"><b>Change Password</b></div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Current Password</label>
                              <input name="old" id="cur" class="form-control"  type="password" placeholder="••••••">
                             
                            </div>
                            <div class="form-group">
                              <input   class="form-control"  name="email"  class="form-control" type="hidden" value="{{Auth::user()->email}}">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>New Password</label>
                              <input class="form-control" name="password" type="password" placeholder="••••••" value="" >
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                              <input class="form-control" id="conf" name="pass" type="password" placeholder="••••••"  value=""></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-5 offset-sm-1 mb-3">
                        <div class="mb-2"><b>Keeping in Touch</b></div>
                        <div class="row">
                          <div class="col">
                            <label>Email Notifications</label>
                            <div class="custom-controls-stacked px-2">
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="notifications-news" checked="">
                                <label class="custom-control-label" for="notifications-news">Newsletter</label>
                              </div>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="notifications-offers" checked="">
                                <label class="custom-control-label" for="notifications-offers">Personal Offers</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col d-flex justify-content-start">
                        <button class="btn btn-primary" type="submit">Change password</button>
                      </div>
                    </div>
                   
                  </form>

                  @if(Session::has('mensaje'))
<p class="text-success">
                  {{ Session::get('mensaje') }}
                 
</p>
                  @endif
                  @if(Session::has('mensajer'))
<p class=text-danger>
                  {{ Session::get('mensajer') }}
                  <script>document.getElementById('conf').style.borderColor="red";</script>
</p>

                  @endif
                  
                  @if(Session::has('merror'))
                  <p class=text-danger>
                  {{ Session::get('merror') }}
                 
                  <script>document.getElementById('cur').style.borderColor="yellow";</script>
                  </p>
                  @endif


                 

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  @elseif(Auth::guard('employee')->check())
  <div class="col">
    <div class="row">
      <div class="col mb-3">
        <div class="card">
          <div class="card-body">
            <div class="e-profile">
              <div class="row">
                <div class="col-12 col-sm-auto mb-3">
                  <div class="mx-auto" style="width: 140px;">
                    <div class="d-flex justify-content-center align-items-center rounded" style="height: 140px; background-color: rgb(233, 236, 239);">
                      <span style="color: rgb(166, 168, 170); font: bold 8pt Arial;">140x140</span>
                    </div>
                  </div>
                </div>
                <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                  <div class="text-center text-sm-left mb-2 mb-sm-0">
                    <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">{{Auth::guard('employee')->user()->firstname}}</h4>
                    <p class="mb-0">{{Auth::guard('employee')->user()->lastname}}</p>
                    <div class="text-muted"><small>Last seen 2 hours ago</small></div>
                    <div class="mt-2">
                      <button class="btn btn-primary" type="button">
                        <i class="fa fa-fw fa-camera"></i>
                        <span>Change Photo</span>
                      </button>
                    </div>
                  </div>
                  <div class="text-center text-sm-right">
                    <span class="badge badge-secondary">{{Auth::guard('employee')->user()->statut}}</span>
                    <div class="text-muted"><small>Joined 09 Dec 2017</small></div>
                  </div>
                </div>
              </div>
              <ul class="nav nav-tabs">
                <li class="nav-item"><a href="" class="active nav-link">Settings</a></li>
              </ul>
              <div class="tab-content pt-3">
                <div class="tab-pane active">
                  <form class="form" method="post" action="{{action('PublicController@editemployeeinfo')}}" >
                  
                    @csrf
                    <div class="row">
                      <div class="col">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>First Name</label>
                              <input class="form-control" type="text" name="firstname" placeholder="{{Auth::guard('employee')->user()->firstname}}" value="{{Auth::guard('employee')->user()->firstname}}">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input class="form-control" type="text" name="lastname" placeholder="{{Auth::guard('employee')->user()->lastname}}" value="{{Auth::guard('employee')->user()->lastname}}">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Email</label>
                              <input disabled  class="form-control"  type="text" value="{{Auth::guard('employee')->user()->email}}">
                              <input    class="form-control"  name="email"  type="hidden" value="{{Auth::guard('employee')->user()->email}}">
                            </div>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col d-flex justify-content-start">
                            <button  class="btn btn-primary" type="submit">Save Changes</button>
                          </div>
                        </div>
                    
                   
                      
                      </div>
                    </div>
                  </form>
                  @if(Session::has('mens'))
                  <p class="text-success">
                                    {{ Session::get('mens') }}
                                   
                  </p>
                                    @endif
                    <br>
                    <form class="form" method="get" action="{{action('PublicController@updateemployee')}}">
                    <div class="row">
                      <div class="col-12 col-sm-6 mb-3">
                        <div class="mb-2"><b>Change Password</b></div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Current Password</label>
                              <input  id="cur" class="form-control" type="password" placeholder="••••••">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>New Password</label>
                              <input  name="password" class="form-control" type="password" placeholder="••••••">
                            </div>
                          </div>
                          <div class="form-group">
                            <input   class="form-control"  name="email"  class="form-control" type="hidden" value="{{Auth::guard('employee')->user()->email}}">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                              <input class="form-control" id='conf'  name="pass" type="password" placeholder="••••••"></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-5 offset-sm-1 mb-3">
                        <div class="mb-2"><b>Keeping in Touch</b></div>
                        <div class="row">
                          <div class="col">
                            <label>Email Notifications</label>
                            <div class="custom-controls-stacked px-2">
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="notifications-news" checked="">
                                <label class="custom-control-label" for="notifications-news">Newsletter</label>
                              </div>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="notifications-offers" checked="">
                                <label class="custom-control-label" for="notifications-offers">Personal Offers</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col d-flex justify-content-start">
                        <button class="btn btn-primary" type="submit">Change password</button>
                      </div>
                    </div>
                    <br>

                    @if(Session::has('mensaje'))
                    <p class="text-success">
                                      {{ Session::get('mensaje') }}
                                     
                    </p>
                                      @endif
                                      @if(Session::has('mensajer'))
                    <p class=text-danger>
                                      {{ Session::get('mensajer') }}
                                      <script>document.getElementById('conf').style.borderColor="red";</script>
                    </p>
                    
                                      @endif
                                      
                                      @if(Session::has('merror'))
                                      <p class=text-danger>
                                      {{ Session::get('merror') }}
                                     
                                      <script>document.getElementById('cur').style.borderColor="yellow";</script>
                                      </p>
                                      @endif

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
      @elseif(Auth::guard('admin')->check())
      <div class="col">
        <div class="row">
          <div class="col mb-3">
            <div class="card">
              <div class="card-body">
                <div class="e-profile">
                  <div class="row">
                    <div class="col-12 col-sm-auto mb-3">
                      <div class="mx-auto" style="width: 140px;">
                        <div class="d-flex justify-content-center align-items-center rounded" style="height: 140px; background-color: rgb(233, 236, 239);">
                          <span style="color: rgb(166, 168, 170); font: bold 8pt Arial;">140x140</span>
                        </div>
                      </div>
                    </div>
                   
                    <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                      <div class="text-center text-sm-left mb-2 mb-sm-0">
                        <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">{{Auth::guard('admin')->user()->firstname}}</h4>
                        <p class="mb-0">{{Auth::guard('admin')->user()->lastname}}</p>
                        <div class="text-muted"><small>Last seen 2 hours ago</small></div>
                        <div class="mt-2">
                          <button class="btn btn-primary" type="button">
                            <i class="fa fa-fw fa-camera"></i>
                            <span>Change Photo</span>
                          </button>
                        </div>
                      </div>
                      <div class="text-center text-sm-right">
                        <span class="badge badge-secondary">{{Auth::guard('admin')->user()->statut}}</span>
                        <div class="text-muted"><small>Joined 09 Dec 2017</small></div>
                      </div>
                    </div>
                  </div>
                  <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="" class="active nav-link">Settings</a></li>
                  </ul>
                  <div class="tab-content pt-3">
                    <div class="tab-pane active">
                      <form class="form" method="get" action="{{action('PublicController@editadmininfo')}}" >
                      
                        @csrf
                        <div class="row">
                          <div class="col">
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <label>First Name</label>
                                  <input class="form-control" type="text" name="firstname" placeholder="{{Auth::guard('admin')->user()->firstname}}" value="{{Auth::guard('admin')->user()->firstname}}">
                                </div>
                              </div>
                              <div class="col">
                                <div class="form-group">
                                  <label>Last Name</label>
                                  <input class="form-control" type="text" name="lastname" placeholder="{{Auth::guard('admin')->user()->lastname}}" value="{{Auth::guard('admin')->user()->lastname}}">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <label>Email</label>
                                  <input disabled  class="form-control" type="text"  value="{{Auth::guard('admin')->user()->email}}">
                                  <input    class="form-control"  name="email"  type="hidden" value="{{Auth::guard('admin')->user()->email}}">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col d-flex justify-content-start">
                                <button  class="btn btn-primary" type="submit">Save Changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      @if(Session::has('mens'))
                      <p class="text-success">
                                        {{ Session::get('mens') }}
                                       
                      </p>
                                        @endif
                        <br>
                        <form class="form" method="get" action="{{action('PublicController@updateadmin')}}">
                        <div class="row">
                          <div class="col-12 col-sm-6 mb-3">
                            <div class="mb-2"><b>Change Password</b></div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <label>Current Password</label>
                                  <input id="cur"  class="form-control"  type="password" placeholder="••••••">
                                </div>
                              </div>
                              <div class="form-group">
                                <input   class="form-control"  name="email"  class="form-control" type="hidden" value="{{Auth::guard('admin')->user()->email}}">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <label>New Password</label>
                                  <input class="form-control" name="password" type="password" placeholder="••••••">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                                  <input class="form-control" id="conf"  name="pass" type="password" placeholder="••••••"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-sm-5 offset-sm-1 mb-3">
                            <div class="mb-2"><b>Keeping in Touch</b></div>
                            <div class="row">
                              <div class="col">
                                <label>Email Notifications</label>
                                <div class="custom-controls-stacked px-2">
                                  <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="notifications-news" checked="">
                                    <label class="custom-control-label" for="notifications-news">Newsletter</label>
                                  </div>
                                  <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="notifications-offers" checked="">
                                    <label class="custom-control-label" for="notifications-offers">Personal Offers</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col d-flex justify-content-start">
                            <button class="btn btn-primary" type="submit">Change password</button>
                          </div>
                        </div>
                      </form>
                     
                  @if(Session::has('mensaje'))
                  <p class="text-success">
                                    {{ Session::get('mensaje') }}
                                   
                  </p>
                                    @endif
                                    @if(Session::has('mensajer'))
                  <p class=text-danger>
                                    {{ Session::get('mensajer') }}
                                    <script>document.getElementById('conf').style.borderColor="red";</script>
                  </p>
                  
                                    @endif
                                    
                                    @if(Session::has('merror'))
                                    <p class=text-danger>
                                    {{ Session::get('merror') }}
                                   
                                    <script>document.getElementById('cur').style.borderColor="yellow";</script>
                                    </p>
                                    @endif
    
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif
          
      <div class="col-12 col-md-3 mb-3">
        <div class="card mb-3">
          <div class="card-body">
            <div class="px-xl-3">
              
              <a  href="{{ route('logout') }}"   class="btn btn-block btn-danger"id="logout" onclick="event.preventDefault();	document.getElementById('logout-form').submit();">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
                </i> Log out</a>
              </a>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <h6 class="card-title font-weight-bold">Support</h6>
            <p class="card-text">Get fast, free help from our friendly assistants.</p>
          <a href="{{url('/contact')}}" type="button" class="btn btn-primary">Contact Us</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
@endsection
