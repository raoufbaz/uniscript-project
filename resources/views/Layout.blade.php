<?php
use App\Cart;
use App\Product as Product;
use App\category as Category;
use App\wishlist;
if(Auth::guard('web')->check())
{
    $user = Auth::user();
    $Cart=Session::get('cart');
    if($Cart!=null)
    {
        DB::table('cart_perms')->where('iduser', '=', $user->id)->delete();
        $cart = new Cart($Cart);
        $Products = $cart->items;
        if($Products!=null)
        {
            foreach($Products as $Product) {
                DB::table('cart_perms')->insert(
                ['iduser' =>  $user->id, 'idprod' => $Product['item']['id'], 'qty' =>$Product['qty']]
                );
            }
        }
    }
    $Wishlist=Session::get('Wishlist');
    if($Wishlist!=null)
    {
        DB::table('wish_lists')->where('iduser', '=', $user->id)->delete();
        $wish = new Wishlist($Wishlist);
        $Products = $wish->items;
        if($Products!=null)
        {
            foreach($Products as $Product) {
                DB::table('wish_lists')->insert(
                ['iduser' =>  $user->id, 'idprod' => $Product['item']['id']]
                );
            }
        }
    }
}
?>


<html>	
	<head>
		
		<title>UniScript</title>
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="{{ URL::asset('/') }}scripts/popper/popper.min.js"></script>
		<link rel="stylesheet" href="{{ URL::asset('/') }}scripts/bootstrap-4.3.1-dist/css/bootstrap.min.css">
		<script src="{{ URL::asset('/') }}scripts/jQuery/jquery-3.3.1.min.js"></script>
		<script src="{{ URL::asset('/') }}scripts/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="{{ URL::asset('/') }}scripts/fontawesome-free-5.12.1-web/css/all.css">
		<link rel="stylesheet" href="{{ URL::asset('/') }}styles/style.css"/>
		<style></style>
	
    </head>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">
		
		<div class="vertical-nav active" style="overflow-y:auto;overflow-x:hidden;" id="sidebar">
            <div class="row">
				<div class="col-9">
				    <div class="btn-group float-left my-4 mx-3">
				        <button type="button" class="btn btn-white btn-outline-dark active lightbtn px-2" onclick="activate_light_theme();"><i class="fas fa-sun px-2" style="font-size:1.3em;" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-white btn-outline-dark darkbtn px-2" onclick="activate_dark_theme();"><i class="fas fa-moon px-2" style="font-size:1.3em;" aria-hidden="true"></i></button>
						
					</div>
					
				</div>
				<div class="col-9">
					<div style="float:right;right:0;position:absolute;top:-60px">
				        <button  class="btn btn btn-outline-secondary" onclick="changeLanguage('fr');">FR</button>
				        <button  class="btn  btn btn-outline-secondary" onclick="changeLanguage('en');">EN</button>
                    </div>
                
		        <button id="col-2 sidebarCollapse2" type="button" class="btn btn-white float-right m-2">
			        <i class="fas fa-2x fa-close"></i>
				</button>
	            </div>
			</div>
			
		    <ul class="nav flex-column  ">
		

			    <li class="nav-item my-2">
					
				
			    <a href="{{url('/')}}" class="nav-link text-dark font-italic" id="home">
					    <i class="fas fa-home text-dark mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">@lang('home.home_p')</font>	
				    </a>
			    </li>
			    <li class="nav-item my-2">
				    <a href="{{url('Products/0/no-order/no-price/no-rating')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-th text-danger mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">@lang('home.prod')</font>	
				    </a>
				</li>
				
				@if(Auth::guard('web')->check())
				<li class="nav-item my-2">
				    <a href="{{url('bills')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-file-invoice text-primary mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Bill History</font>	
				    </a>
				</li>

				<li class="nav-item my-2">
				    <a href="{{url('reservationclient')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-hourglass-start text-success mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Reservation</font>	
				    </a>
				</li>
				
				@elseif(Auth::guard('employee')->check())
			
				<li class="nav-item my-2">
				    <a href="{{url('members')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-users text-primary mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Members</font>	
				    </a>
				</li>
				
				</li>
				<li class="nav-item my-2">
				    <a href="{{url('invoice')}}" class="nav-link text-dark font-italic" id="Products">
						<i class="fas fa-file-invoice text-success mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Invoice</font>	
				    </a>
				</li>
				<li class="nav-item my-2">
				    <a href="{{url('reservation')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-hourglass-start text-success mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Reservation</font>	
				    </a>
				</li>
				
				@elseif(Auth::guard('admin')->check())
				<li class="nav-item my-2">
				    <a href="{{url('stock')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-cubes text-secondary mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Stock</font>	
				    </a>
				</li>
				<li class="nav-item my-2">
				    <a href="{{url('members')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-users text-primary mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Members</font>	
				    </a>
				</li>
				
				<li class="nav-item my-2">
				    <a href="{{url('employees')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-user-tie text-info mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Employees</font>	
				    </a>
				</li>
				
				
				<li class="nav-item my-2">
				    <a href="{{url('invoice')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-file-invoice text-success mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Invoice</font>	
				    </a>
				</li>
				<li class="nav-item my-2">
				    <a href="{{url('reservation')}}" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-hourglass-start text-success mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Reservation</font>	
				    </a>
				</li>
			
				@else
		
				@endif



			    <li class="nav-item my-2">
				    <a href="{{ url('about') }}" class="nav-link text-dark font-italic" id="about">
					    <i class="fas fa-address-card text-info mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">@lang('home.about_p')</font>	
				    </a>
			    </li>
			    <li class="nav-item my-2">
				    <a href="{{ url('contact') }}" class="nav-link text-dark font-italic" id="contact">
					    <i class="fas fa-phone text-success mr-3" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">@lang('home.contact_p')</font>	
				    </a>
			    </li>
		    </ul>
		</div>
		<div class="page-content active" id="content" style="min-height: 100vh;">
			<nav id="navbar">
				
				<div class="nav-item bg-white">
					<div class="media row m-0 align-items-center">
					
						<div class="col-lg-3 col-md-9 col-sm-9 col-9">
							<button id="sidebarCollapse" type="button" class="btn btn-white float-left  mr-2">
								<i class="fas fa-2x fa-bars"></i> 
							</button>
							<a class="btn p-0 m-0" href="{{ url('/') }}"><font color="gray" style="font-size:1.8em">Uni</font><font color="red" style="font-size:1.8em">Script</font></a>
							
						</div>
						
                        <div class="col-lg-1 col-md-3 col-sm-3 col-3 mb-4">
						
				            @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
                            <a href="{{ url('Wishlist') }}" class="btn btn-outline-white float-right m-0 p-0 border-0" id="wishlist">
					            <span class="badge badge-white text-danger mt-2 ml-4 py-0 px-1 Wishlist-items" style="font-size:1.1em;">{{Session::has('Wishlist') ? Session::get('Wishlist')->totalQty : ''}}</span><br><i class="fas fa-2x fa-star text-warning p-0 m-0" ></i>	
				            </a>
                            @endif
						</div>
						
						<form onsubmit="return false;" class="input-group col-lg-4 col-md-11 col-sm-11 col-11 m-auto p-0  d-flex justify-content-between">
							<div class="input-group-btn search-panel">
								<button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown">
									<span id="search_concept">@lang('home.all')</span> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu cats" data-idcat="0" role="menu">
								
    
								</ul>
							</div>
							<input type="text" autocomplete="off" class="form-control searchbox" name="Product" placeholder="@lang('home.search')..."  >
							<span class="input-group-btn">
								<button class="btn btn-outline-info searchbtn" type="submit"><i class="fas fa-search p-1"></i></button>
							</span>
                            <br/>
                            <table class="table table-hover bg-white" style="position: absolute;top: 100%;width: 100%;z-index:3" id="livesearch"></table>
						</form>
						
                        <div class="col-lg-1 col-md-4 col-sm-4 col-4 mb-4">
                        @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
				            <a href="{{ url('cart') }}" class="btn btn-outline-white float-left m-0 p-0 border-0" id="cart">
					            <span class="badge badge-white text-danger mt-2 ml-4 py-0 px-1 cart-items" style="font-size:1.1em;">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span><br><i class="fas fa-2x fa-shopping-cart text-success p-0 m-0" ></i>	
				            </a>
                            @endif
			            </div>
                        @if(Auth::guard('web')->check())
						<div class="col-lg-3 col-md-6 col-sm-6 col-6 p-0 ">
							<div class="media btn btn-outline-success d-flex align-items-left float-right m-2 p-0 rounded-pill" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="images/users/{{Auth::user()->id}}.jpg" onerror="this.onerror=null; this.src='https:\/\/s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg'"  alt="..." width="55" class="m-0 p-0 rounded-circle img-thumbnail shadow-sm notif-img">
								<div class="media-body mx-3">
									<h4 class="m-0 " style="word-wrap: break-word;">{{Auth::user()->firstname}}</h4>
									<i class="font-weight-light text-muted mb-0">regular</i>
									<div class="dropdown-menu dropdown-menu-right mt-2">
										<a onclick="window.location.href='{{url('profile')}}'" class="dropdown-item" ><i class="fas fa-eye mr-2"></i> View profile</a>
										<a class="dropdown-item" onclick="window.location.href='{{url('profile')}}'"><i class="fas fa-edit mr-2 text-info"></i> Edit</a>
										<a href="{{ route('logout') }}" class="dropdown-item" id="logout" onclick="event.preventDefault();	document.getElementById('logout-form').submit();">
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
											</form>
											<i class="fas fa-sign-out-alt mr-2 text-danger"></i> Log out</a>
										</a>
									</div>
								</div>
							</div>
						</div>

						@elseif(Auth::guard('employee')->check())
						<div class="col-lg-3 col-md-6 col-sm-6 col-6 p-0 ">
							<div class="media btn btn-outline-primary d-flex align-items-left float-right m-2 p-0 rounded-pill" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg"  alt="..." width="55" class="m-0 p-0 rounded-circle img-thumbnail shadow-sm notif-img">
								<div class="media-body mx-3">
									<h4 class="m-0 " style="word-wrap: break-word;">{{Auth::guard('employee')->user()->firstname}}</h4>
									<i class="font-weight-light text-muted mb-0">Employee</i>
									<div class="dropdown-menu dropdown-menu-right mt-2">
										<a class="dropdown-item" onclick="window.location.href='{{url('profile')}}'"><i class="fas fa-eye mr-2"></i> View profile</a>
										<a class="dropdown-item" onclick="window.location.href='{{url('profile')}}'"><i class="fas fa-edit mr-2 text-info"></i> Edit</a>
										<a href="{{ route('logout') }}" class="dropdown-item" id="logout" onclick="event.preventDefault();	document.getElementById('logout-form').submit();">
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
											</form>
											<i class="fas fa-sign-out-alt mr-2 text-danger"></i> Log out</a>
										</a>
									</div>
								</div>
							</div>
						</div>
				@elseif(Auth::guard('admin')->check())
				<div class="col-lg-3 col-md-6 col-sm-6 col-6 p-0 ">
					<div class="media btn btn-outline-danger d-flex align-items-left float-right m-2 p-0 rounded-pill" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg"  alt="..." width="55" class="m-0 p-0 rounded-circle img-thumbnail shadow-sm notif-img">
								<div class="media-body mx-3">
							<h4 class="m-0 " style="word-wrap: break-word;">{{Auth::guard('admin')->user()->firstname}}</h4>
							<i class="font-weight-light text-muted mb-0">Admin</i>
							<div class="dropdown-menu dropdown-menu-right mt-2">
								<a class="dropdown-item" onclick="window.location.href='{{url('profile')}}'"><i class="fas fa-eye mr-2"></i> View profile</a>
								<a class="dropdown-item" onclick="window.location.href='{{url('profile')}}'"><i class="fas fa-edit mr-2 text-info"></i> Edit</a>
								<a href="{{ route('logout') }}" class="dropdown-item" id="logout" onclick="event.preventDefault();	document.getElementById('logout-form').submit();">
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
									<i class="fas fa-sign-out-alt mr-2 text-danger"></i> Log out</a>
								</a>
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-3 col-md-8 col-sm-8 col-8 p-0">
					<button class="btn btn-outline-info text-center float-right m-2 p-2 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" ><i class="fas fa-sign-in-alt" style="font-size:1.4em;">@lang('home.log')</i><span class="caret"></span></button>
					<ul class="dropdown-menu dropdown-menu-right mt-2" id="form1">
						<li id="f1" class="px-0 py-2">
							<form class="form m-4" role="form" method="post" name="form" action="{{ route('login') }}">
								@csrf
								<h4 class="text-center text-dark">@lang('home.member')</h4>
								<div class="form-group">
									<input id="email" type="email" name="email" class="form-control form-control-sm mx-auto"  autocomplete="username" placeholder="@lang('home.user')">
									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									 @enderror
								</div>
								<div class="form-group">
									<input  id="password" type="password" name="password"  class="form-control form-control-sm mx-auto" autocomplete="current-password" placeholder="@lang('home.pass')">
									@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								   @enderror
								</div>
								<div class="form-group">
									<center><button type="submit" class="btn btn-success mt-2 mt-sm-0">@lang('home.log')</button></center>
								</div>
								<div class="form-group text-center">
								   <a href="#" onclick="showEmployeesForm()" style="color:#0898ff;">@lang('home.your') ? </a>
								</div>
							</form>
						</li>
						<li id="f2" style="display:none" class="px-0 py-2">
							<form class="form m-4" role="form" method="post" name="form2" action="{{ url("login/employee") }}">
								@csrf
								<h4 class="text-center text-dark">Employee</h4>
								<div class="form-group">
									<input id="email" type="email" name="email" class="form-control form-control-sm mx-auto"  autocomplete="username" placeholder="@lang('home.user')">
									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									 @enderror
								</div>
								<div class="form-group">
									<input  id="password" type="password" name="password" type="password" class="form-control form-control-sm mx-auto" name=""  autocomplete="current-password" placeholder="@lang('home.pass')">
									@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								   @enderror
								</div>
								<div class="form-group">
									<center><button type="submit" class="btn btn-success mt-2 mt-sm-0">@lang('home.log')</button></center>
								</div>
							</form>
						</li>
						<li style="display:none" id="f3" class="px-0 py-2">
							<form class="form m-4" role="form" method="post" name="form3" action="{{ url("login/admin") }}">
								@csrf
								<h4 class="text-center text-dark">Admin</h4>
								<div class="form-group">
									<input id="email" type="email" name="email" class="form-control form-control-sm mx-auto"  autocomplete="username" placeholder="@lang('home.user')">
									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									 @enderror
								</div>
								<div class="form-group">
									<input  id="password" type="password" name="password" type="password" class="form-control form-control-sm mx-auto" name=""  autocomplete="current-password" placeholder="@lang('home.pass')">
									@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								   @enderror
								</div>
								<div class="form-group">
									<center><button type="submit" class="btn btn-success mt-2 mt-sm-0">@lang('home.log')</button></center>
								</div>
								<div class="form-group text-center">
								   <a href="#" onclick="showUserForm()" style="color:#0898ff;">@lang('home.Are you a user ?') </a>
								</div>
							</form>
						</li>
					</ul>
				</div>
			@endif
							
					</div>
				</div>
			</nav>
			<div class="container-fluid bg-light p-5 all" style="min-height:100vh" >
			
				@yield('content')

				
			</div>
            <div id="snackbar" class="rounded-pill"></div>
            <div id="snackbar2" class="rounded-pill"></div>
			<footer class="sticky-bottom bg-light">
				<center>&copy;2020 - UniScript TECH</center>
			</footer>
		</div>
	</body>
	
	<script>


	$(document).ready(function(){
                
                
				$(window).click(function() {
					$('#sidebar, #content').addClass('active');
                     
				});
                $('.searchbtn').click(function() {
                    var id =  $('.cats').data('idcat');
                    var name = $('.searchbox').val();
					url = '{{ url("Products/id/no-order/no-price/no-rating/name") }}';
                url = url.replace('id', id);
				url = url.replace('name', name);
					window.location.href = url;
				});
		        $.ajax({
                type: "get",
                async:true,
		        url: "{{ url('Layoutcat') }}",
		        success: function(data){
			        data.Categories.forEach(function(item){


					var lang="{{ Session::get('locale') }}";
					
						if(lang=="fr"){
                    cat="<li><div class='nav-link btn-outline-dark font-italic catclick' id='"+item.id+"'  data-idcat='"+item.id+"'>"+item.namefr+"</div></li>";
					$( ".cats" ).append(cat);}
					    else if(lang=="en"){
							cat="<li><div class='nav-link btn-outline-dark font-italic catclick' id='"+item.id+"'  data-idcat='"+item.id+"'>"+item.name+"</div></li>";
					$( ".cats" ).append(cat);

						}
						else{
							cat="<li><div class='nav-link btn-outline-dark font-italic catclick' id='"+item.id+"'  data-idcat='"+item.id+"'>"+item.name+"</div></li>";
					$( ".cats" ).append(cat);
						}
						
			        });
			        all=" <hr/><li><div class='nav-link btn-outline-dark font-italic catclick' data-idcat='0'>@lang('home.all')</div></li>";
			        $( ".cats" ).append( all );
                    }
                });
                $('.searchbox').blur(function() {
                   $('#livesearch').html("");
                });
                $('.searchbox').focus(function() {
                   $('.searchbox').keyup();
                });
                $('.searchbox').keyup(delay(function (e) {
                    var minlength = 1;
                    var value=$(this).val();
					var id =   $('.cats').attr("data-idcat");
                    if (value.length >= minlength ) {
                    $.ajax({
                    type : 'get',
                    url : '{{URL::to('SearchSuggestions')}}',
                    data:{'search':value,'searchcat':id},
                    beforeSend: function(){
                        $('#livesearch').html('<tbody class="border"><tr><td class="Product text-muted text-center m-auto p-4"><i class="fas fa-2x fa-spinner fa-spin"></i></td></tr></tbody>');
                    },
                    success:function(data){
                    $('#livesearch').html(data);
                    }
                    });
                    }
                    else{
                        $('#livesearch').html("");
                    }
			    },500));
                $(document).on("click", '.catclick', function(event) { 
                       var name = $(this).text();
					   var id =  $(this).data('idcat'); 
					   $('.search-panel span#search_concept').text(name);
                       $('.cats').attr("data-idcat",id);
                    });
			});
			$(function() {
			  $('#sidebarCollapse').on('click', function() {
			  event.stopPropagation();
				$('#sidebar, #content').removeClass('active');
			  });
			  $('#sidebarCollapse2').on('click', function() {
				$('#sidebar, #content').addClass('active');
			  });
			});  
            function search(name,id) {

				url = '{{ url("Products/id/no-order/no-price/no-rating/name") }}';
                url = url.replace('id', id);
				url = url.replace('name', name);
				window.location.href = url;
	         
            }
 function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
function addToCart(id) {
            var url="{{route('Product.addToCart',['id' =>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
            type: "get",
            async:true,
		    url: url,
		    success: function(data){
            $('.cart-items').text(data.items);
                   $('#snackbar').html(data.snackbar);
                  notify();
			    }
            });
          }
function changeLanguage(lang) {
var url="{{route('Product.changeLanguage',['lang' =>':lang'])}}";
url = url.replace(':lang', lang);
$.ajax({
type: "get",
async:true,
url: url,
success: function(data){
location.reload();
}
});
}
function addToWishlist(id) {
            var url="{{route('Product.addToWishlist',['id' =>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
            type: "get",
            async:true,
		    url: url,
		    success: function(data){
            $('.Wishlist-items').text(data.items);
                   $('#snackbar2').html(data.snackbar);
                  notify2();
			    }
            });
          }
 function notify() {
         var x = document.getElementById('snackbar');
         x.className = "rounded-pill show";
        setTimeout(function(){
                   x.className = x.className.replace("show", ""); }, 3000);
    }
    function notify2() {
         var x = document.getElementById('snackbar2');
         x.className = "rounded-pill show";
        setTimeout(function(){
                   x.className = x.className.replace("show", ""); }, 3000);
    }
    var Mode = getCookie("DarkMode");
                    if (Mode!="" && Mode=="ON")
		            {
			            $('.theme').prop('checked', true);
        	            activate_dark_theme();
		            }
                    else
                    {
			            $('.theme').prop('checked', false);
			            activate_light_theme();
		            }
                    function activate_dark_theme()
	                {
                         $('.lightbtn').removeClass('active');
                         $('.darkbtn').addClass('active');
		                var css = 
		                'html,img {filter: invert(95%) hue-rotate(180deg)}',
		                head = document.getElementsByTagName('head')[0],
		                style = document.createElement('style');
		                style.type = 'text/css';
		                if (style.styleSheet){
		                style.styleSheet.cssText = css;
		                } 
		                else {
		                style.appendChild(document.createTextNode(css));
		                }
		                head.replaceChild(style,document.getElementsByTagName('style')[0]);
                        setCookie("DarkMode","ON","5");
	                }
	                function activate_light_theme()
	                {
                        $('.darkbtn').removeClass('active');
                         $('.lightbtn').addClass('active');
		                var css = 'html {-webkit-filter: invert(0%);' +
		                '-moz-filter: invert(0%);' + 
		                '-o-filter: invert(0%);' + 
		                '-ms-filter: invert(0%); }',
		                head = document.getElementsByTagName('head')[0],
		                style = document.createElement('style');
		                style.type = 'text/css';
		                if (style.styleSheet){
		                style.styleSheet.cssText = css;
		                } 
		                else {
		                style.appendChild(document.createTextNode(css));
		                }
		                head.replaceChild(style,document.getElementsByTagName('style')[0]);	
                        setCookie("DarkMode","OFF","5");
                    }
                    
		            function setCookie(cname, cvalue, exdays) {
		              var d = new Date();
		              d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		              var expires = "expires="+d.toUTCString();
		              document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		            }
		            function getCookie(cname) {
		                  var name = cname + "=";
		                  var decodedCookie = decodeURIComponent(document.cookie);
		                  var ca = decodedCookie.split(';');
		                  for(var i = 0; i <ca.length; i++) {
			                var c = ca[i];
			                while (c.charAt(0) == ' ') {
			                  c = c.substring(1);
			                }
			                if (c.indexOf(name) == 0) {
			                  return c.substring(name.length, c.length);
			                }
		                  }
		                  return "";
					 }
					 function showUserForm(){
						$('#f1').show();
						$('#f2').hide();
						$('#f3').hide();
					 }
					 function showEmployeesForm(){
						$('#f1').hide();
						$('#f2').show();
						$('#f3').show();
					}

		</script>
</html>
