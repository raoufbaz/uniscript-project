@extends('Layout')

@section('content')

<div class="container py-5">
	<div class="row min-vh-50 align-items-center py-5">
	  <div class="col-lg-6">
		<h1 class="display-4">@lang('home.about_p')</h1>
		<p class="lead text-muted mb-0"> </p>
	  </div>
	  <div class="col-lg-6 d-none d-lg-block"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556834136/illus_kftyh4.png" alt="" class="img-fluid"></div>
	</div>
</div>

<div class="bg-white py-5">
  <div class="container py-5">
	<div class="row align-items-center mb-5">
	  <div class="col-lg-6 order-2 order-lg-1"><i class="fa fa-bar-chart fa-2x mb-3 text-primary"></i>
		<h2 class="font-weight-light">@lang('home.See our latest Products')</h2>
		<p class="font-italic text-muted mb-4">@lang('home.see all our Products, their prices and discounts available easily from anywhere and anytime').</p>
	  </div>
	  <div class="col-lg-5 px-5 mx-auto order-1 order-lg-2"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556834139/img-1_e25nvh.jpg" alt="" class="img-fluid mb-4 mb-lg-0"></div>
	</div>
	<div class="row align-items-center">
	  <div class="col-lg-5 px-5 mx-auto"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556834136/img-2_vdgqgn.jpg" alt="" class="img-fluid mb-4 mb-lg-0"></div>
	  <div class="col-lg-6"><i class="fa fa-leaf fa-2x mb-3 text-primary"></i>
		<h2 class="font-weight-light">@lang('home.Our services')</h2>
		<p class="font-italic text-muted mb-4">@lang('home.ex').</p>
	  </div>
	</div>
  </div>
</div>

<div class="bg-light py-5">
  <div class="container-fluid py-1">
	<div class="row mb-4">
	  <div class="col-lg-5">
		<h2 class="display-4 font-weight-light">@lang('home.team')</h2>
		<p class="font-italic text-muted">@lang('home.We are available on social media, follow us!')</p>
	  </div>
	</div>
	<div class="row text-center justify-content-center">
	  <div class="col-xl-3 col-md-3 col-sm-6 mb-5 p-2">
		<div class="bg-white rounded shadow-sm py-5 p-0"><img src="https://scontent.fykz2-1.fna.fbcdn.net/v/t1.0-9/93062010_921799191573545_1559843138774761472_n.jpg?_nc_cat=106&_nc_sid=85a577&_nc_ohc=ETiMruLiJbIAX-P-Fl1&_nc_ht=scontent.fykz2-1.fna&oh=88ab28295aa6672967f0867687c03ba8&oe=5EBC9FD1" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
		  <h5 class="mb-0">Boukedira Salah Eddine</h5><span class="small text-uppercase text-muted">Fullstack-Developer</span>
		  <ul class="social mb-0 list-inline mt-3">
			<li class="list-inline-item"><a href="https://www.facebook.com/play.rapboy" class="social-link"><i class="fab fa-facebook-f text-primary"></i></a></li>
			<li class="list-inline-item"><a href="https://twitter.com/RayoSalah" class="social-link"><i class="fab fa-twitter text-info"></i></a></li>
			<li class="list-inline-item"><a href="https://www.instagram.com/salah.edd_/" class="social-link"><i class="fab fa-instagram text-danger"></i></a></li>
			<li class="list-inline-item"><a href="#" class="btn disabled social-link"><i class="fab fa-linkedin"></i></a></li>
		  </ul>
		</div>
	  </div>
	   <div class="col-xl-3 col-md-3 col-sm-6 mb-5 p-2">
		<div class="bg-white rounded shadow-sm py-5 p-0"><img src="images/users/1.jpg" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
		  <h5 class="mb-0">Raouf Baaziz </h5><span class="small text-uppercase text-muted">Fullstack-Developer</span>
		  <ul class="social mb-0 list-inline mt-3">
			<li class="list-inline-item"><a href="https://www.facebook.com/raouf.whiz" class="social-link"><i class="fab fa-facebook-f text-primary"></i></a></li>
			<li class="list-inline-item "><a href="#" class="btn disabled social-link"><i class="fab fa-twitter text-info"></i></a></li>
			<li class="list-inline-item"><a href="https://www.instagram.com/raouf.baz/" class="social-link"><i class="fab fa-instagram text-danger"></i></a></li>
			<li class="list-inline-item"><a href="https://www.linkedin.com/in/raouf-baaziz-74676a176/" class="btn  social-link"><i class="fab fa-linkedin"></i></a></li>
		  </ul>
		</div>
	  </div>
	   <div class="col-xl-3 col-md-3 col-sm-6 mb-5 p-2">
		<div class="bg-white rounded shadow-sm py-5 p-0"><img src="images/users/2.jpg" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
		  <h5 class="mb-0">Malek Saidana</h5><span class="small text-uppercase text-muted">Fullstack-Developer</span>
		  <ul class="social mb-0 list-inline mt-3">
			<li class="list-inline-item"><a href="https://www.facebook.com/malek.saiidana" class="social-link"><i class="fab fa-facebook-f text-primary"></i></a></li>
			<li class="list-inline-item"><a href="#"  class="btn disabled social-link"><i class="fab fa-twitter text-info"></i></a></li>
			<li class="list-inline-item"><a href="https://www.instagram.com/malek_saidana/" class="social-link"><i class="fab fa-instagram text-danger"></i></a></li>
			<li class="list-inline-item"><a href="https://www.linkedin.com/in/malek-saidana-621a63180/" class="social-link"><i class="fab fa-linkedin"></i></a></li>
		  </ul>
		</div>
	  </div>
	  <div class="col-xl-3 col-md-3 col-sm-6 mb-5 p-2">
		<div class="bg-white rounded shadow-sm py-5 p-0"><img src="https://scontent.fykz2-1.fna.fbcdn.net/v/t1.0-9/p960x960/39060308_2061526383866121_8432914620632530944_o.jpg?_nc_cat=110&_nc_sid=85a577&_nc_ohc=ltXmyLMSf7sAX-0nOEr&_nc_ht=scontent.fykz2-1.fna&_nc_tp=6&oh=493a68be4271d7e6219d69f679abbd98&oe=5EBD29CC" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
		  <h5 class="mb-0">Achraf Jahidi </h5><span class="small text-uppercase text-muted">Fullstack-Developer</span>
		  <ul class="social mb-0 list-inline mt-3">
			<li class="list-inline-item"><a href="https://www.facebook.com/play.rapboy" class="social-link"><i class="fab fa-facebook-f text-primary"></i></a></li>
			<li class="list-inline-item"><a href="https://twitter.com/RayoSalah" class="social-link"><i class="fab fa-twitter text-info"></i></a></li>
			<li class="list-inline-item"><a href="https://www.instagram.com/salah.edd_/" class="social-link"><i class="fab fa-instagram text-danger"></i></a></li>
			<li class="list-inline-item"><a href="#" class="btn disabled social-link"><i class="fab fa-linkedin"></i></a></li>
		  </ul>
		</div>
	  </div>
	   
	</div>
  </div>
</div>	

@endsection
