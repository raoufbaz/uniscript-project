@extends('Layout')

@section('content')

<div class="container" id="accordion">
    @if ($Sales->count()== 0)
    
        <h1> You have no Reservation</h1>
    @else
    @php ($i = 1)
    {{-- @php ($itemid=1) --}}
    @php ($date=null)
    @php ($idold=0)
    @php ($discold=0)
    @foreach ($Sales as $invoice)
    @if ($invoice->datereserved == $date) 
    {{-- @php ($itemid++) --}}
   <script>
     var t ="#"+ {{$idold}};
      $(t).append("<tr><td class=\"center\">{{--$itemid--}}</td>{{-- {{$invoice->Product->name}} --}}<td class=\"left strong\">{{substr($invoice->Product->name,0,20)}}...</td><td class=\"left\">{{substr($invoice->Product->desc,0,40)}}...</td><td class=\"right\">{{$invoice->Product->price}}</td><td class=\"center\">{{$invoice->qty}}</td><td class=\"right\">{{$invoice->Product->price*$invoice->qty}}</td></tr>");
      //subtotal
      var s = "#sub"+{{$idold}};
      var sub = parseFloat($(s).text().substring(1));
      var subt = {{$invoice->Product->price}}+sub;
      $(s).text("$"+parseFloat(({{$invoice->Product->price}}+sub)).toFixed(2).toString()) ;
    
      //discount montant
    var disct = parseFloat(subt*{{$invoice->discount/100}}).toFixed(2);
      var dm = "#discmontant"+{{$idold}};
      $(dm).text("$"+(parseFloat(subt*{{$invoice->discount/100}})).toFixed(2).toString()) ;
      //tax
      var tax= "#tax"+{{$idold}};
      var taxt =parseFloat(subt*0.15).toFixed(2);
      $(tax).text("$"+(parseFloat(subt*0.15)).toFixed(2).toString()) ;
    //total
    var tot= "#tot"+{{$idold}};
    $(tot).text("$"+(parseFloat(subt-disct+parseFloat(taxt)).toFixed(2).toString()));

  </script>
    @else
    @php ($idold=$i)
    @php ($discold=$invoice->discount)

    <div class="card">
      <div class="card-header" id="heading{{$i}}" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
      Reservation
      <strong>{{$invoice->datereserved}}</strong> 
        <span class="float-right"> <strong>Status:</strong> Pending</span>
      </div>
      @if($i ==1)
      <div id="collapse{{$i}}" class="collapse show" aria-labelledby="heading{{$i}}" data-parent="#accordion">
      @else
      <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}" data-parent="#accordion">
      @endif
          <div class="card-body">
      <div class="row mb-4">
      <div class="col-sm-6">
      <h6 class="mb-3">From:</h6>
      <div>
      <strong>Uniscript</strong>
      </div>
      <div>123 de la Gauchetiere, Montreal</div>
      <div>Email: info@uniscript.com</div>
      <div>Phone: +514 123 1234</div>
      </div>
      
      <div class="col-sm-6">
      <h6 class="mb-3">To:</h6>
      <div>
      <strong>{{Auth::user()->firstname}} {{Auth::user()->lastname}}</strong>
      </div>
      <div>123 rue st-Michel, Montreal</div>
      
      <div>{{Auth::user()->email}}</div>
      <div>Phone: +514-123-1234</div>
      </div>
      
      
      
      </div>
      
      <div class="table-responsive-sm">
      <table id="{{$i}}" class="table table-striped">
      <thead>
      <tr>
      <th class="center">#</th>
      <th>Item</th>
      <th>Description</th>
      
      <th class="right">Unit Cost</th>
        <th class="center">Qty</th>
      <th class="right">Total</th>
      </tr>
      </thead>
      <tbody>
      <tr>
      <td class="center">{{--$itemid--}}</td>
      {{-- {{dd($invoice->Product->name)}} --}}
      <td class="left strong">{{substr($invoice->Product->name,0,20)}}...</td>
      <td class="left">{{substr($invoice->Product->desc,0,40)}}...</td>
      
      <td class="right">{{$invoice->Product->price}}</td>
        <td class="center">{{$invoice->qty}}</td>
      <td class="right">{{$invoice->Product->price*$invoice->qty}}</td>
      </tr>
      </tbody>
      </table>
      </div>
      <div class="row">
      <div class="col-lg-4 col-sm-5">
      
      </div>
      
      <div class="col-lg-4 col-sm-5 ml-auto">
      <table class="table table-clear">
      <tbody>
      <tr>
      <td class="left">
      <strong>Subtotal</strong>
      </td>
    <td id="sub{{$i}}" class="right">${{number_format($invoice->subtotal,2)}}</td>
      </tr>
      <tr>
      <td  class="left">
      <strong><p id="discpourcent{{$i}}">Discount </p></strong>
      </td>
    <td id="discmontant{{$i}}" class="right">$ {{number_format($invoice->discount,2)}}</td>
      </tr>
      <tr>
      <td class="left">
       <strong>TAX (15%)</strong>
      </td>
      <td id="tax{{$i}}"  class="right">${{number_format(($invoice->subtotal- $invoice->discount)*0.15,2)}}</td>
      </tr>
      <tr>
      <td class="left">
      <strong>Total</strong>
      </td>
      <td id="tot{{$i}}" class="right">
      <strong>${{number_format(($invoice->subtotal- $invoice->discount)*1.15,2)}}</strong>
      </td>
      </tr>
      </tbody>
      </table>
      
      </div>
      
      </div>
      
      </div>
        </div>
      </div>
    @endif
    @php ($date=$invoice->datereserved)
        <?php $i++;?>
    @endforeach
    @endif
  @endsection
