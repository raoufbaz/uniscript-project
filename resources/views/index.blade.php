
@inject('Comment', 'App\Comment')
@inject('Rating', 'App\Rating')
@inject('Sale', 'App\Sale')
        @extends('Layout')
		@section('content')
		<div class="row m-0">
			<div class="col-12 mx-auto text-center">
                <div class="big-title text-center">
		            <h3 class="big_title">@lang('home.welcome_p')</h3>
			        <p class="title-para">@lang('home.choose')</p>
                </div>
             </div>
             <div class="m-auto load-cats p-5">
                <div class="spinner-grow text-success m-1"></div>
                 <div class="spinner-grow text-dark m-1"></div>
                 <div class="spinner-grow text-primary m-1"></div>
             </div>

			<div class="MultiCarousel carousel-cats d-none" data-items="1,2,3,4" data-slide="1" id="MultiCarousel"  data-interval="1000">
				<div class="MultiCarousel-inner">
                    <a class="item text-center m-auto p-3 box" style="cursor:pointer" href="Products/0/no-order/no-price/no-rating" data-idcat="0">
						<img class="img-fluid rounded-pill shadow" src="https://www.forwardtofba.com/blog/wp-content/uploads/2017/07/mont_various_fpo.jpg" />
					    <div class="box-content"><h3 class="title">@lang('home.all_cat')</h3></div>
					</a>
					@foreach ($Categories as &$cat) 
					<a class="item text-center m-auto p-3 box" style="cursor:pointer" href="{{url('Products/'.$cat->id.'/no-order/no-price/no-rating')}}" data-idcat="{{$cat->id}}">
						<img class="img-fluid rounded-pill shadow" src="{{asset('images/Categories/'.$cat->id.'.jpg')}}" />
					    <div class="box-content"><h3 class="title">{{ __('home.'.$cat->name.'') }}</h3></div>
					</a>
					@endforeach
				</div>
				<button class="btn leftLst btn-outline-danger border-0 rounded-pill"><i class="fa fa-2x fa-chevron-left py-5" aria-hidden="true"></i></button>
				<button class="btn rightLst btn-outline-danger border-0 rounded-pill"><i class="fa fa-2x fa-chevron-right py-5" aria-hidden="true"></i></button>
			</div>	
		</div>
		<div class="row bg-white rounded shadow-sm m-0 p-0">
			<div class="col-12 mx-auto text-center">
			    <h2>@lang('home.trend') <b>@lang('home.prod')</b></h2>
			</div>
            <div class="m-auto load-prods p-5">
                 <div class="spinner-grow text-warning m-1"></div>
                 <div class="spinner-grow text-info m-1"></div>
                 <div class="spinner-grow text-danger m-1"></div>
             </div>
			<div class="MultiCarousel carousel-prods d-none" data-items="1,2,3,4" data-slide="1" id="MultiCarousel"  data-interval="1000">
				<div class="MultiCarousel-inner">
					@foreach ($Products as &$prod)
                    <div class="item m-auto p-3" >
	                    <figure class="card card-Product shadow">
                            <div  class="badge p-0 text-white bg-warning" style="position:absolute;top:0;right:0;z-index:1;font-size:1.3em;"><i class="fa fa-star p-1"> {{number_format((float)$Rating->where('idprod','=', $prod->Product->id)->avg('stars'), 1, '.', '')?:'0'}}</i></div>
                            <div  class="badge p-0 text-white bg-success" style="position:absolute;top:0;left:0;z-index:1;font-size:1.3em;"><i class="fas fa-dollar-sign p-1"> {{$prod->Product->price}}</i></div>
		                        
		                    <div class="img-wrap" onclick="redirect({{$prod->id}})"><img src="{{asset('images/Products/'.$prod->id.'.jpg')}}"></div>
		                    <figcaption class="info-wrap" onclick="redirect({{$prod->id}})">
				                    <h4 class="title"><script>
											var lang="{{ Session::get('locale') }}";
										if(lang=='fr'){
									document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->namefr )}}").substr(0,19) + "...");}
									else if(lang=='en'){
										document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->name )}}").substr(0,19) + "...");
									}
									else{
										document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->name )}}").substr(0,19) + "...");
									
									}

									</script></h4>
				                    <p class="desc"><script>
									var lang="{{ Session::get('locale') }}";
										if(lang=='fr'){
									document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->descfr )}}").substr(0,45) + "...");
										}
										else if(lang=='en'){
											document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->desc )}}").substr(0,45) + "...");


										}
										else{document.write(("{{preg_replace( "/\r|\n/", "", $prod->Product->desc )}}").substr(0,45) + "...");}
									
									</script></p>
				                    <div class="rating-wrap">
					                    <i class="label-rating">{{$Comment->where('idprod','=', $prod->Product->id)->count()}} @lang('home.review(s)')</i>
					                    <i class="label-rating">{{$Sale->where('idprod','=', $prod->Product->id)->count()}} @lang('home.order(s)') </i>
				                    </div> 
		                    </figcaption>
		                    @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
                            <div class="bottom-wrap row m-0">
			                    <button onclick="addToCart('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-6 btn btn-white text-success float-left border-0 add-to-cart rounded"><i class="fa fa-2x fa-cart-plus"></i></button>	
			                    <button onclick="addToWishlist('{{$prod->id}}')" data-idprod="{{$prod->id}}" class="col-6 btn btn-white float-right border-0 add-to-wishlist rounded"><i class="fa fa-2x fa-star star-wishlist" style=" " aria-hidden="true"></i></i></button>	
		                    </div>
                            @endif
	                    </figure>
                    </div>

					@endforeach
				</div>
				<button class="btn btn-outline-warning border-0  leftLst"><i class="fa fa-2x fa-chevron-left py-5" aria-hidden="true"></i></button>
				<button class="btn btn-outline-warning border-0 rightLst"><i class="fa fa-2x fa-chevron-right py-5" aria-hidden="true"></i></button>
			</div>	
		</div>
	<script>

		$(document).ready(function(){
        
				var itemsMainDiv = ('.MultiCarousel');
				var itemsDiv = ('.MultiCarousel-inner');
				var itemWidth = "";
                
				$('.leftLst, .rightLst').click(function () {
					var condition = $(this).hasClass("leftLst");
					if (condition)
						click(0, this);
					else
						click(1, this)
				});
				ResCarouselSize();
				$(window).resize(function () {
					ResCarouselSize();
				});
				function ResCarouselSize() {
                $('.carousel-cats').removeClass('d-none');
                    $('.load-cats').addClass('d-none');
                    $('.carousel-prods').removeClass('d-none');
                    $('.load-prods').addClass('d-none');
					var incno = 0;
					var dataItems = ("data-items");
					var itemClass = ('.item');
					var id = 0;
					var btnParentSb = '';
					var itemsSplit = '';
					var sampwidth = $(itemsMainDiv).width();
					var bodyWidth = $('body').width();
					$(itemsDiv).each(function () {
						id = id + 1;
						var itemNumbers = $(this).find(itemClass).length;
						btnParentSb = $(this).parent().attr(dataItems);
						itemsSplit = btnParentSb.split(',');
						$(this).parent().attr("id", "MultiCarousel" + id);


						if (bodyWidth >= 1200) {
							incno = itemsSplit[3];
							itemWidth = sampwidth / incno;
						}
						else if (bodyWidth >= 992) {
							incno = itemsSplit[2];
							itemWidth = sampwidth / incno;
						}
						else if (bodyWidth >= 768) {
							incno = itemsSplit[1];
							itemWidth = sampwidth / incno;
						}
						else {
							incno = itemsSplit[0];
							itemWidth = sampwidth / incno;
						}
						$(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
						$(this).find(itemClass).each(function () {
							$(this).outerWidth(itemWidth);
						});

						$(".leftLst").addClass("over");
						$(".rightLst").removeClass("over");

					});
				}

				function ResCarousel(e, el, s) {
					var leftBtn = ('.leftLst');
					var rightBtn = ('.rightLst');
					var translateXval = '';
					var divStyle = $(el + ' ' + itemsDiv).css('transform');
					var values = divStyle.match(/-?[\d\.]+/g);
					var xds = Math.abs(values[4]);
					if (e == 0) {
						translateXval = parseInt(xds) - parseInt(itemWidth * s);
						$(el + ' ' + rightBtn).removeClass("over");

						if (translateXval <= itemWidth / 2) {
							translateXval = 0;
							$(el + ' ' + leftBtn).addClass("over");
						}
					}
					else if (e == 1) {
						var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
						translateXval = parseInt(xds) + parseInt(itemWidth * s);
						$(el + ' ' + leftBtn).removeClass("over");

						if (translateXval >= itemsCondition - itemWidth / 2) {
							translateXval = itemsCondition;
							$(el + ' ' + rightBtn).addClass("over");
						}
					}
					$(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
				}
				function click(ell, ee) {
					var Parent = "#" + $(ee).parent().attr("id");
					var slide = $(Parent).attr("data-slide");
					ResCarousel(ell, Parent, slide);
				}
                
		});
        
        function redirect(id) {
			url = '{{ url("Product_infos/id") }}';
                url = url.replace('id', id);
				window.location.href = url;
        }
        
         
	</script>
@endsection
