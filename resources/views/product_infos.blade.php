@extends('Layout')

@section('content')


<div class="container-fluid bg-white rounded min-vh-100 shadow p-4 m-0">
	<div class="bg-light">
		<div class="row">
			<div class="bg-white col-lg-5 col-sm-12">
				<div class="row m-2">
					<div class="prv col-md-12">
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"><img src="{{asset('images/Products/'.$Product->id.'.jpg')}}" /></div>
						  <div class="tab-pane" id="pic-2"><img src="{{asset('images/Products/'.$Product->id.'_1.jpg')}}" /></div>
						  <div class="tab-pane" id="pic-3"><img src="{{asset('images/Products/'.$Product->id.'_2.jpg')}}" /></div>
						  <div class="tab-pane" id="pic-4"><img src="{{asset('images/Products/'.$Product->id.'_3.jpg')}}" /></div>
						  <div class="tab-pane" id="pic-5"><img src="{{asset('images/Products/'.$Product->id.'_4.jpg')}}" /></div>
						</div>
					</div>
                    <div class="col-md-12 m-0 p-0">
						<ul class="preview-thumbnail justify-content-center row nav nav-tabs border-0">
						  <li class="active mx-1 p-0 col-2"><a data-target="#pic-1" data-toggle="tab"><img src="{{asset('images/Products/'.$Product->id.'.jpg')}}" class="rounded" /></a></li>
						  <li class="col-2 mx-1 p-0"><a data-target="#pic-2" data-toggle="tab"><img src="{{asset('images/Products/'.$Product->id.'_1.jpg')}}" class="rounded" /></a></li>
						  <li class="col-2 mx-1 p-0"><a data-target="#pic-3" data-toggle="tab"><img src="{{asset('images/Products/'.$Product->id.'_2.jpg')}}" class="rounded" /></a></li>
						  <li class="col-2 mx-1 p-0"><a data-target="#pic-4" data-toggle="tab"><img src="{{asset('images/Products/'.$Product->id.'_3.jpg')}}" class="rounded" /></a></li>
						  <li class="col-2 mx-1 p-0"><a data-target="#pic-5" data-toggle="tab"><img src="{{asset('images/Products/'.$Product->id.'_4.jpg')}}" class="rounded" /></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="details col-lg-7 col-sm-12 p-5">
				<big style="font-size:1.6em;">
					<script>
						var lang="{{ Session::get('locale') }}";
						if(lang=="fr"){

					document.write("<p>{{$Product->namefr}}</p>");
					}
					else if(lang=="en"){
						document.write("<p>{{$Product->name}}</p>");

						
					}
					else{
						document.write("<p>{{$Product->name}}</p>");

					}
					</script>
					
				</big>
				<div class="row p-2">
					<small class="col-12">
                   @if($Ratings == null)
                       <i class="fa fa-star fa-2x text-muted"></i>
                       <i class="fa fa-star fa-2x text-muted"></i>
                       <i class="fa fa-star fa-2x text-muted"></i>
                       <i class="fa fa-star fa-2x text-muted"></i>
                       <i class="fa fa-star fa-2x text-muted"></i>
                   @else
                       @for ($x = 0; $x < $Ratings; $x++) 
                         <i class="fa fa-star fa-2x text-warning"></i>
                       @endfor
                       @for ($x = 0; $x < (5-$Ratings); $x++)
                         <i class="fa fa-star fa-2x text-muted"></i>
                       @endfor
                   @endif
                   </small>
				   <span class="col-12">({{$Reviews}}) Votes</span>
				<hr>
                </div>
                <h3 class="text-primary m-0">
					{{$Product->price}} <small class="text-muted">+@lang('home.tax')</small>
				</h3>
				@if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
                <div class="row pt-1 pb-4">
					<div class="col-sm-12 col-md-6 col-lg-6 my-4">
						<button onclick="addToCart({{$Product->id}});" class="btn btn-success btn-lg"><i class="fa fa-shopping-cart mr-2"></i>@lang('home.Add to cart')</button>
					</div>
					<div class="col-sm-12 col-md-6 col-lg-6">
						<div class="row float-right prod-opts">
							<button onclick="addToWishlist('{{$Product->id}}')" class="btn btn-outline-warning col-12 p-2 my-1"><i class="fa fa-star"></i> @lang('home.Add to wishlist') </button>
							<button class="btn btn-outline-info col-12 p-2 my-1"><i class="fa fa-envelope"></i> @lang('home.contact_p')</button>
						</div>
					</div>
				</div>
                @endif
				<div class="description description-tabs py-4">
					<ul id="myTab" class="nav nav-tabs justify-content-center" role="tablist">
						<li class="nav-item"><a href="#more-information" data-toggle="tab" class="nav-link active">@lang('home.Product_des') </a></li>
						<li class="nav-item"><a href="#specifications" data-toggle="tab" class="nav-link">@lang('home.Specifications')</a></li>
						<li class="nav-item"><a href="#reviews" onclick="getComments('{{$Product->id}}','no-order')" data-toggle="tab" class="nav-link">@lang('home.Reviews')</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div class="container tab-pane active" id="more-information">
						<br>
							<strong>Description</strong>
							<script>
								var lang="{{ Session::get('locale') }}";
								if(lang=="fr"){

							document.write("<p>{{$Product->descfr}}</p>");
							}
							else if(lang=="en"){
								document.write("<p>{{$Product->desc}}</p>");

								
							}
							else{	document.write("<p>{{$Product->desc}}</p>");}
							</script>
						</div>
						<div class="container tab-pane" id="specifications">
							<br>
							<dl class="">
								<dt>ASIN:</dt>
								<dd>{{$Product->asin}}</dd>
								<br>
								<dt>@lang('home.Date first available at UniScript.ca'):</dt>
								<dd>{{$Product->created_at->format('d-m-Y')}}</dd>
								<br>  
								<dt>@lang('home.Sales'):</dt>
								<dd>{{$Sales}} @lang('home.item(s) sold')</dd>
							</dl>
						</div>
						<div class="container tab-pane" id="reviews">
							<br>
                            <div class="input-group-btn">
                                <button type="button" class="btn text-danger dropdown-toggle" data-toggle="dropdown">
						            <span id="order_concept">@lang('home.Ordered by Date : new to old')</span> <span class="caret"></span>
					            </button>
					            <ul class="dropdown-menu" role="menu">
                                    <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="no-order">@lang('home.Date : new to old')</div></li>
                                    <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-date-asc">@lang('home.Date : old to new')</div></li>
                                    <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-rating-asc">@lang('home.Rating : low to high')</div></li>
                                    <li><div class='nav-link btn-outline-dark font-italic orderclick' data-order="by-rating-desc">@lang('home.Rating : high to low')</div></li>
                                </ul>
				            </div>
                            @if(!(Auth::guard('admin')->check()) && !(Auth::guard('employee')->check()))
							@auth
                            <form onsubmit="return false;" class="pb-2">
								<textarea rows="2" class="form-control comment" placeholder="Write a review"></textarea>
                                <div class="row">
                                <div class="col-md-6 col-12 m-0 p-0 float-left">
                                    <div class='rating-stars text-center m-2 float-left'>
                                        <ul id='stars'>
                                          <li class='star' title='Poor' data-value='1'>
                                            <i class='fa fa-star fa-fw'></i>
                                          </li>
                                          <li class='star' title='Fair' data-value='2'>
                                            <i class='fa fa-star fa-fw'></i>
                                          </li>
                                          <li class='star' title='Good' data-value='3'>
                                            <i class='fa fa-star fa-fw'></i>
                                          </li>
                                          <li class='star' title='Excellent' data-value='4'>
                                            <i class='fa fa-star fa-fw'></i>
                                          </li>
                                          <li class='star' title='WOW!!!' data-value='5'>
                                            <i class='fa fa-star fa-fw'></i>
                                          </li>
                                        </ul>
                                      </div>
                                </div>
								<div class="col-md-6 col-12 m-0 p-0 float-right">
									<button onclick="postcomment('{{$Product->id}}')" class="btn btn-danger m-2 post-comment float-right">
										@lang('home.Submit Review')
									</button>
								</div>
                                @if(Session::has('Commentres'))
                                  <p class="text-success">
                                       {{ Session::get('Commentres') }}
                                  </p>
                                @endif
                                </div>
							</form>
                            @else
                                <h3 class="text-muted text-center p-2"><a href="#" class="text-primary">@lang('home.Sign-in')</a> @lang('home.for more options')</h3>
                            @endauth
                            @endif
							<div class="no-padding">
								<ul class="p-0 comments-body " style="list-style-type:none">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
            $(document).on("click", '.orderclick', function(event) {
                var order = $(this).attr('data-order');
                getComments('{{$Product->id}}',order);
                $('#order_concept').html("Ordered by "+$("div").find("[data-order='"+order+"']").html());
             });
             $(document).on("click", '.page-link', function(event) {
                event.preventDefault();
                var url=$(this).attr('href');
                $.ajax({
                type : 'get',
                url : url,
                beforeSend: function(){
                $('.page-link').html('<li class="text-center text-muted"><i class="fas fa-2x fa-spinner fa-spin"></i><li>');
                },
                success:function(data){
                $('.pagination').remove();
                $('.comments-body').append(data);
                $('.pagination').children(":first").remove();
                $('.page-link').html("@lang('home.Show more')");
                $( ".pagination" ).addClass( "justify-content-center" );
                }
                });
             });
              $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); 
   
                $(this).parent().children('li.star').each(function(e){
                  if (e < onStar) {
                    $(this).addClass('hover');
                  }
                  else {
                    $(this).removeClass('hover');
                  }
                });
    
              }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                  $(this).removeClass('hover');
                });
              });
  
  
              $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); 
                var stars = $(this).parent().children('li.star');
    
                for (i = 0; i < stars.length; i++) {
                  $(stars[i]).removeClass('selected');
                }
    
                for (i = 0; i < onStar; i++) {
                  $(stars[i]).addClass('selected');
                }
        
              });
             
});
function postcomment(id)
                {
                    var rating = parseInt($('#stars li.selected').last().data('value'), 10);
                    var comment=$(".comment").val();
                    var url="{{route('Product.postComment',['id' =>':id','comment' =>':comment', 'rating' =>':rating'])}}";
                    url = url.replace(':id', id);        
                    url = url.replace(':comment', comment);        
                    url = url.replace(':rating', rating);        
                    $.ajax({
                        type : 'get',
                        url : url,
                        beforeSend: function(){
                            $('.post-comment').html('<i class="fas fa-2x fa-spinner fa-spin"></i>');
                        },
                        success:function(data){
                        getComments(id,'no-order');
                         $('.post-comment').html("@lang('home.Submit Review')");
                        }
                        });
                }
            function getComments(id,order)
                {
                    var url="{{route('Product.getComments',['id' =>':id','order' =>':order'])}}";
                    url = url.replace(':id', id);
                    url = url.replace(':order', order);
        
                    $.ajax({
                        type : 'get',
                        url : url,
                        beforeSend: function(){
                            $('.comments-body').html('<li class="text-center text-muted"><i class="fas fa-2x fa-spinner fa-spin"></i></li>');
                        },
                        success:function(data){
                        $('.comments-body').html(data);
                        $('.pagination').children(":first").remove();
                        $('.page-link').html("@lang('home.Show more')");
                        $( ".pagination" ).addClass( "justify-content-center" );
                        }
                        });
                }
    
</script>
@endsection
