@extends('Layout')

@section('content')


<div class="container">
  <h2>Products Stock</h2>
  <div class="row" style="margin-bottom:3%">
  <div class="col-4">
    <button  class="btn btn-success "  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Add</button>
    
  </div>
  <div class="col-4 center">
    <button  class="btn btn-warning" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">Update</button>
    
  </div>
     </div>
     
     <div div class="collapse" id="collapseExample">
      <h3 style="margin-left:40%;margin-bottom:5%"><u>New product</u></h3>
      <form id="add" class="form" method="post" action="{{action('PublicController@addprod')}}">
        @csrf
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Name Eng</label>
                  <input required class="form-control firstname" type="text" name="nameeng" placeholder="name" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Name Fr</label>
                  <input required class="form-control lastname" type="text" name="namefr" placeholder="nom" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Description Eng</label>
                  <textarea  class="form-control" id="desceng" name="desceng" placeholder="English description" rows="6" required></textarea>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Description Fr</label>
                  <textarea class="form-control" id="descfr" name="descfr" placeholder="French description" rows="6" required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>ASIN</label>
                  <input required class="form-control firstname" type="text" name="asin" placeholder="B07YDNWP3A" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Price</label>
                  <input required class="form-control firstname" type="decimal" name="price" placeholder="0.00" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Quantity</label>
                  <input required class="form-control lastname" type="text" name="qty" placeholder="1" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="catid" >
                    @foreach ($cat as $c)
                    <option value="{{$c->id}}">{{substr($c->name,0,40)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
           
           
            
            <div class="row">
                 <div class="col d-flex justify-content-start">
                     <button  class="btn btn-primary" type="submit">Add Product</button>
                 </div>
            </div>
            @if(Session::has('addres'))
              <p class="text-success">
                   {{ Session::get('addres') }}
              </p>
            @endif
        </form>
        
     </div>

     <div div class="collapse" id="collapseExample2">
      <h3 style="margin-left:40%;margin-bottom:5%"><u>Update product</u></h3>
      <form id="update" class="form" method="post" action="{{action('PublicController@updateprod')}}">
        @csrf
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Product ID</label>
                  <select class="form-control" name="id" >
                    @foreach ($all as $w)
                    <option value="{{$w->id}}">{{substr($w->name,0,40)}}...</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Name Eng</label>
                  <input class="form-control firstname" type="text" name="nameeng" placeholder="name" value="">
                </div>
              </div>
              
              <div class="col">
                <div class="form-group">
                  <label>Name Fr</label>
                  <input class="form-control lastname" type="text" name="namefr" placeholder="nom" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Description Eng</label>
                  <textarea class="form-control" id="desceng" name="desceng" placeholder="English description" rows="6" ></textarea>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Description Fr</label>
                  <textarea class="form-control" id="desceng" name="descfr" placeholder="French description" rows="6" ></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>ASIN</label>
                  <input class="form-control firstname" type="text" name="asin" placeholder="B07YDNWP3A" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Price</label>
                  <input class="form-control firstname" step="1" min="0" type="number" name="price" placeholder="0.00" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Quantity</label>
                  <input class="form-control lastname" type="text" name="qty" placeholder="1" value="">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="catid" >
                    @foreach ($cat as $c)
                    <option value="{{$c->id}}">{{substr($c->name,0,40)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
           
           
            
            <div class="row">
                 <div class="col d-flex justify-content-start">
                     <button  class="btn btn-primary" type="submit">Update product</button>
                 </div>
            </div>
            @if(Session::has('addres'))
              <p class="text-success">
                   {{ Session::get('addres') }}
              </p>
            @endif
        </form>
        
     </div>
  <div class="input-group col-lg-4 col-md-11 col-sm-11 col-11 m-auto p-0  d-flex justify-content-between">
   
        <input type="text" autocomplete="off" class="form-control search" id="search" name="search" placeholder="Search..."  >
        <span class="input-group-btn">
          <button class="btn btn-outline-info searchb" type="button"><i class="fas fa-search p-1"></i></button>
        </span>
                      
                  <br/>
                  <table class="table table-hover bg-white" style="position: absolute;top: 100%;width: 100%;z-index:3" id="livesearch"></table>
  </div>                                                                                   
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
        <th> Quantity </th>
        <th> View </th>
        <th> Delete </th>
    </tr>
    </thead>
    <tbody>
      @foreach ($Products as $s)
      <tr>
        <td>{{$s->id}}</td>
        <td>{{$s->name}} </td>
        <td>{{$s->desc}}</td>
        <td>{{$s->price}}</td>
        <td>{{ $s->quantity }}</td>
        <td><a href="#" role="button" onclick="redirect({{$s->id}})" class="nav-link text-dark font-italic">
          <i class="fas fa-eye text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
        <td><a href="{{route('delete_stock', ['id'=>$s->id]) }}" class="nav-link text-dark font-italic">
          <i class="fas fa-times text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
       </tr>
      @endforeach
    </tbody>
  </table>
 
  </div>
</div>
{{ $Products->onEachSide(1)->links() }}

<script>


$(".searchb").click(function(){
 
 
 url="{{url('stocksearch/id')}}";
 url = url.replace('id',  $('#search').val());
 window.location.href = url;
});


function redirect(id) {
	             url = '{{ url("Product_infos/id") }}';
                url = url.replace('id', id);
				window.location.href = url;
	
}

</script>

<script>
    $( ".pagination" ).addClass( "justify-content-center" );
    var pathArray = (window.location.pathname).substring(window.location.pathname.indexOf("Products/")).split('/');
    var catid = pathArray[1];
    var orderby = pathArray[2];
    var price = pathArray[3];
    var rating = pathArray[4];
    var searchterm="";
    if(pathArray[5])
    {
        var searchterm = pathArray[5];
        searchterm = decodeURI(searchterm);
    }
    $('.searchbox').val(searchterm);
    if(orderby!="no-order")
    {
        $('#order_concept').html("@lang('home.Ordered by') "+$("div").find("[data-order='"+orderby+"']").html());
    }
    if(price!="no-price")
    {
        $('.sliderprice').val(price);
        $('.priceselected').html(price);
    }
    if(rating!="no-rating")
    {
       $("div").find("[data-rating='"+rating+"']").addClass('active');
    }
    var ProductsViewMode = getCookie("ProductsViewMode");
    if (ProductsViewMode!="" && ProductsViewMode=="grid")
	{
		grid();
	}
    else
    {
		list();
	}
	$(document).ready(function(){
             cat="<option value='0'>@lang('home.all')</option>";
             $( ".filtercats" ).append( cat );
             $.ajax({
                type: "get",
                async:true,
		        url: "{{ url('Layoutcat') }}",
		        success: function(data){
			        data.Categories.forEach(function(item){
                        var lang="{{ Session::get('locale') }}";
					
                    if(lang=="fr"){
                        cat="<option value='"+item.id+"'>"+item.namefr+"</option>";
		     	        $( ".filtercats" ).append( cat );
               }
                    else if(lang=="en"){
                        cat="<option value='"+item.id+"'>"+item.name+"</option>";
		     	        $( ".filtercats" ).append( cat );

                    }
                    else{
                        cat="<option value='"+item.id+"'>"+item.name+"</option>";
		     	        $( ".filtercats" ).append( cat );
                    }
                    
			        });
                    },
                    complete: function (data) {
                       $(".filtercats").val(catid);
                       $('.search-panel span#search_concept').text($(".filtercats").find(":selected").text());
                       $('.cats').attr("data-idcat",catid);
                     }
             });
            
             $(document).on("click", '.orderclick', function(event) {
                    var order = $(this).attr('data-order');
					url = '{{ url("Products/id/order/price/rating/name") }}';
					url = url.replace('id', catid);
					url = url.replace('name', searchterm);
					url = url.replace('price', price);
					url = url.replace('rating', rating);
                    url = url.replace('order', order);
					window.location.href = url;
             });
            
});



</script>
  @endsection