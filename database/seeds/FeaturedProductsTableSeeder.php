<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FeaturedProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('featured_products')->insert([
            [
            'id' => '1',
            ],
            [
            'id' => '2',
            ],
            [
            'id' => '3',
            ],
            [
            'id' => '4',
            ],
            [
            'id' => '5',
            ],
            [
            'id' => '6',
            ],
             [
            'id' => '7',
            ],
            [
            'id' => '8',
            ]
                ]);
    }
}
