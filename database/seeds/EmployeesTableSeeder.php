<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            
            [
                'id' => '1',
                'firstname' => 'employee',
                'lastname' => 'employee',
                'email'=> 'employee@gmail.com',
                'statut'=>'employee',
                'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
                ]
                    ]);
    }
}
