<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class); 
        $this->call(ProductsTableSeeder::class); 
        $this->call(FeaturedProductsTableSeeder::class); 
        $this->call(DiscountsTableSeeder::class); 
        $this->call(SalesTableSeeder::class); 
        $this->call(RatingsTableSeeder::class); 
        $this->call(CommentsTableSeeder::class);
        $this->call(VisitorTableSeeder::class);  
        $this->call(EmployeesTableSeeder::class);  
        $this->call(AdminsTableSeeder::class);  
    
        
    }
}
