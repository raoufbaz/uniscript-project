<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
            'id' => '1',
            'iduser' => '1',
            'idprod' => '1',
            'comment'=> 'best gaming set ever!!'
            ],
            [
            'id' => '2',
            'iduser' => '2',
            'idprod' => '1',
            'comment'=> 'wow i\'m so glad i purshased it.'
            ],
            [
            'id' => '3',
            'iduser' => '3',
            'idprod' => '1',
            'comment'=> 'mediocre quality. you get what you\'ve paid for...'
            ],
            [
            'id' => '4',
            'iduser' => '4',
            'idprod' => '1',
            'comment'=> 'seems bad.'
            ]
         ]);
    }
}
