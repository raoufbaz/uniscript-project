<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'id' => '1',
            'firstname' => 'raouf',
            'lastname' => 'baaziz',
            'birthdate'=> '1997-05-11',
            'email'=> 'raoufbaaziz@outlook.com',
            'statut'=>'User',
            'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ],
            [
                'id' => '2',
                'firstname' => 'malek',
                'lastname' => 'saidana',
                'birthdate'=> '1997-05-11',
                'email'=> 'maleksaidana@gmail.com',
                'statut'=>'User',
                'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ],
            [
                'id' => '3',
                'firstname' => 'user',
                'lastname' => 'user',
                'birthdate'=> '1999-10-18',
                'email'=> 'user@gmail.com',
                'statut'=>'User',
                'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ],
            [
                'id' => '4',
                'firstname' => 'salah eddine',
                'lastname' => 'boukedira',
                'birthdate'=> '1999-10-18',
                'email'=> 'rayo.walo2@gmail.com',
                'statut'=>'User',
                'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ],
            [
            'id' => '5',
            'firstname' => 'achraf',
            'lastname' => 'jahidi',
            'birthdate'=> '2000-01-01',
            'email'=> 'achrafjahidi@gmail.com',
            'statut'=>'User',
            'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ],
            [
            'id' => '10',
            'firstname' => 'no',
            'lastname' => 'user',
            'birthdate'=> '2000-01-01',
            'email'=> 'nouser@gmail.com',
            'statut'=>'none',
            'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ]
                ]);
    }
}
