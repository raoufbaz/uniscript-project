<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
            'id' => '1',
            'name' => 'Redragon S101 Gaming Keyboard and Mouse Combo, RGB LED Backlit 104 Keys, Wrist Rest, 6 Button Mouse for Windows PC',
            'namefr'=>'Redragon S101 Clavier de Jeu et une Souris Combo, RGB LED Rétro-éclairé , Repose-poignet, 6 boutons Souris pour Windows PC ',
            'descfr'=>'CLAVIER DE Jeu ET SOURIS COMBO : Inclus Redragon RGB Rétro-éclairé , Clavier de jeu et une souris RGB Rétro-éclairé. TOUT-EN-UN PC JOUEUR VALEUR KIT , FANTASTIQUE POUR LES JOUEURS  ',
            'desc' => 'GAMING KEYBOARD AND GAMING MOUSE COMBO: Includes Redragon RGB Backlit Gaming Keyboard and RGB Backlit Gaming Mouse. ALL-IN-ONE PC GAMER VALUE KIT, Fantastic for Gamers',
            'asin'=> 'B07YDNWP3A',
            'price'=> '29.99',
            'quantity'=> '100',
            'idcat'=> '1'
            ],
            [
            'id' => '2',
            'name' => 'PICTEK Gaming Mouse Wired [7200 DPI] [Programmable] [Breathing Light] Ergonomic Game USB Computer Mice RGB Gamer Desktop Laptop PC Gaming Mouse, 7 Buttons',
            'namefr'=>'PICTEK Souris de Jeu avec Fil [7200 DPI] [Programmable] [RESPIRATION LUMIÈRE] Ergonomique USB Computer Souris RGB Joueur Bureau Laptop PC Souris de Jeu , 7 boutons  ',
            'descfr'=>'【7200 DPI 5 Niveaux Ajustables】Pictek Programmable Souris de Jeu supporte un très large rang de DPI de 500 à 7200 . Par défaut cinq DPI niveaux : 1200/2400/3500/5500/7200.Avec deux DPI boutons',
            'desc' => '【7200 DPI 5 Levels Adjustable】Pictek Programmable Gaming Mouse supports an extremely wide range of DPI from 500 to 7200. Default five DPI levels: 1200/2400/3500/5500/7200. With two DPI button',

            'asin'=> 'F78UAJWO3B',
            'price'=> '19.99',
            'quantity'=> '100',
            'idcat'=> '1'
            ],
            [
            'id' => '3',
            'name' => 'ALONG FIT High Waisted Leggings with Pockets for Women Workout Tummy Control Leggings',
            'namefr'=>'DES BAS  Ajustés LE LONG  de LA COUPE , taille haute avec des poches pour les femmes ,entraînement Tummy pour le Contrôle des jambes  ',
            'descfr'=>'SENSATION CONFORTABLE : Les pantalons du  yoga sont cousus du tissu de qualité supérieure glissant comme du beurre, doux mais puissant pendant les entraînements. Ces bas de yoga taille haute vous offrent une expérience confortable ',
            'desc' => 'COMFORTABLE FEELING: The yoga pants sewn of premium fabric glide on like butter, soft yet empowering during workouts. These high waisted yoga leggings provide you with a comfortable experience',
            'asin'=> 'D89HSOPX9V',
            'price'=> '19.99',
            'quantity'=> '100',
            'idcat'=> '2'
            ],
            [
            'id' => '4',
            'name' => 'WearMe Pro - Reflective Lens Round Trendy Sunglasses',
            'namefr'=>' WearMe Pro - Lunettes de soleil tendance à verres réfléchissants',
            'descfr'=>'100% UV400 PROTECTION- WearMe Pro\'s les lunettes de soleil sont équipées de lentilles HD qui peuvent filtrer les reflets du soleil. Les lentilles WearMe Pro protègent vos yeux des dommages à long terme  par le blocage nuisible ',
            'desc' => '100% UV400 PROTECTION - WearMe Pro\'s sunglasses come equipped with HD lenses that can filter out sunlight glare. WearMe Pro lenses protect your eyes from long term damage by blocking harmful',
            'asin'=> 'V88PDNWP3F',
            'price'=> '22.99',
            'quantity'=> '100',
            'idcat'=> '2'
            ],
            [
            'id' => '5',
            'name' => 'Henley Shirts for Men Short Sleeve Cotton Button T-Shirts Slim Fit',
            'namefr'=>'T-shirts boutonnés à manches courtes en coton pour hommes',
            'descfr'=>'MATÉRIEL: Choisir une côte de bébé en coton de qualité supérieure 1x1 pour confectionner cette chemise de poule ultra douce. Confortable, durable et avec une bonne élasticité.95% coton 5% élasthanne \ r \ nLAVAGE: Lavage en machine à froid doucement, sécher',
            'desc' => 'MATERIAL: Picking premium cotton 1x1 baby rib to make this ultra soft henly shirt. Cozy,durable and with good elasticity.95%cotton 5%spandex\r\nWASH: Machine wash cold softly, trumble dry in lo',
            'asin'=> 'D88CBNWP8F',
            'price'=> '19.99',
            'quantity'=> '100',
            'idcat'=> '2'
            ],
            [
            'id' => '6',
            'name' => "Acer Aspire 5 15.6'' Laptop - Silver (Intel Core i3-10110U/256GB SSD/8GB RAM/Windows 10)",
            'namefr'=>"Ordinateur portable Acer Aspire 5 15,6'- Argent (Intel Core i3-10110U / SSD 256 Go / RAM 8 Go / Windows 10)",
            'descfr'=>"Un appareil puissant dans un design élégant et portable, cet ordinateur portable Acer Aspire maintient votre niveau de confiance élevé lors de vos déplacements. Il dispose d'un processeur Intel Core i3-10110U et de 8 Go de RAM DDR4 pour offrir une puissance exceptionnelle pour gérer facilement des tâches complexes. Son écran Full HD de 15,6 pouces avec un design à lunette mince peut donner une expérience visuelle de cinéma.",
                'desc' => "A powerful device in a sleek and portable design, this Acer Aspire laptop keeps your confidence level high whileon the go. It features an Intel Core i3-10110U processor and 8GB DDR4 RAM to offer exceptional power to easily handle complex tasks. Its 15.6-inch full HD screen with slim bezel design can give a cinema-like visual experience.",
                'asin'=> 'B07YDSJP8B',
                'price'=> '550.49',
                'quantity'=> '100',
                'idcat'=> '3'
                ],
            
            [
            'id' => '7',
            'name' => "Insignia 15.24m (50ft.) Cat6 Ethernet Cable (NS-PNW5650-C)",
            'namefr'=>"Câble Ethernet Cat6 de 15,24 m (50 pi) d'Insignia (NS-PNW5650-C)",
            'descfr'=>"Ce câble Ethernet réseau de 50 pouces d'Insignia offre une vitesse de transfert de 1000 Mbps. Il a deux types d'extrémité RJ45 et un placage de connecteur doré.",
            'desc' => "This 50' network ethernet cable from Insignia features a transfer speed of 1000Mbps. It has two RJ45 end types and gold connector plating.",
            'asin'=> 'M08DNWS6P',
            'price'=> '59.99',
            'quantity'=> '100',
            'idcat'=> '4'
            ],

            [
                'id' => '8',
                'name' => 'HP DeskJet 3755 Wireless All-In-One Inkjet Printer - Seagrass',
                'namefr'=>"Imprimante à jet d'encre tout-en-un sans fil HP DeskJet 3755 - Seagrass",
                'descfr'=>"Ne vous laissez pas berner par la taille de cette imprimante jet d'encre couleur tout-en-un sans fil HP DeskJet 3755; c'est un cheval de bataille pour n'importe quel bureau à domicile. Il vous permet d'économiser de l'espace sur votre bureau tout en offrant la puissance dont vous avez besoin pour rester productif. Le Wi-Fi intégré permet une impression sans effort via votre réseau sans fil, tandis que l'application mobile HP permet une impression à distance pratique via votre appareil mobile.",
                'desc' => "Don't be fooled by the size of this HP DeskJet 3755 wireless colour all-in-one inkjet printer; it's a workhorse for any home office. It saves you space on your desktop while offering the power you need to stay productive. Built-in Wi-Fi makes for effortless printing via your wireless network, while HP's mobile app allows for handy remote printing via your mobile device.",
                'asin'=> 'B07YDNWP3F',
                'price'=> '91.24',
                'quantity'=> '100',
                'idcat'=> '4'
                ],
        [
                'id' => '9',
                'name' => 'PS4 Controller Charger, OIVO Dualshock 4 Controller USB Charging Station Dock with LED Light Indicators, Playstation 4 Charging Station for Sony Playstation4 / PS4 / PS4 Slim / PS4 Pro Contro',
                'namefr'=>'Chargeur de contrôleur PS4, station de charge USB OIVO Dualshock 4 Controller avec indicateurs lumineux LED, station de charge Playstation 4 pour Sony Playstation4 / PS4 / PS4 Slim / PS4 Pro Contro',
                'descfr'=>"【SYSTÈME DE CHARGE RAPIDE】 Charge de 2 heures à double amortisseur 4 contrôleurs en même temps. Plus besoin d'attendre plus de temps pour charger deux manettes PS4, économisez votre temps et amusez-vous davantage. \ R \ n 【MULTI POWER SU】",
                'desc' => '【FAST CHARGING SYSTEM】 2-hour charging dual shock 4 controllers at the same time. No need to wait more time when charge two PS4 controllers, save your time and have more fun.\r\n【MULTI POWER SU】',
                'asin'=> 'B22UYMNL9F',
                'price'=> '18.99',
                'quantity'=> '100',
                'idcat'=> '1'
                ],

                [
                    'id' => '10',
                    'name' => 'SWITCH CONTROLLERS NEON PINK/NEON GREEN NINTEN - Left and Right Edition',
                    'namefr'=>'COMMUTATEURS CONTRÔLLEURS NÉON ROSE / VERT NÉON NINTEN - Édition Gauche et Droite',
                    'descfr'=>" Deux Joy-Con peuvent être utilisés indépendamment dans chaque main ou ensemble comme un seul contrôleur de jeu lorsqu'ils sont attachés à la poignée Joy-Con \ r \ nIls peuvent également être attachés à la console principale pour une utilisation en mode portable",
                    'desc' => 'Two Joy-Con can be used independently in each hand or together as one game controller when attached to the Joy-Con grip\r\nThey can also attach to the main console for use in handheld mode ',
                    'asin'=> 'B08UIORT1P',
                    'price'=> '99.96',
                    'quantity'=> '100',
                    'idcat'=> '1'
                    ],
                    
                    [
                        'id' => '11',
                        'name' => 'Bitdefender Family Pack (PC/Mac/iOS/Android) - 15 Users - 1 Year',
                        'namefr'=>"Pack famille Bitdefender (PC / Mac / iOS / Android) - 15 utilisateurs - 1 an",
                        'descfr'=>"Le Bitdefender Family Pack est une protection unique contre les cybermenaces pour chaque appareil de votre maison. Bitdefender VPN permet une expérience de navigation sécurisée et privée, tandis que Bitdefender Safepay améliore la sécurité des services bancaires en ligne et des achats en ligne. Le contrôle parental Premium vous aide à surveiller et à personnaliser les appareils de vos enfants.",
                        'desc' => "The Bitdefender Family Pack is an ultimate one-stop protection against cyber threats for every device in your house. Bitdefender VPN enables a safe and private browsing experience, while the Bitdefender Safepay improves the security of online banking and e-shopping. Premium Parental Control helps you monitor and customize your kids' devices.",
                        'asin'=> 'N82YNHXDT3F',
                        'price'=> '99.99',
                        'quantity'=> '100',
                        'idcat'=> '4'
                        ]

                    ]);
    }
}
