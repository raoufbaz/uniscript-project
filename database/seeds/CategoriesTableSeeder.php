<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
            'id' => '1',
            'name' => 'Gaming',
            'namefr'=>'Jeu',

            ],
            [
            'id' => '2',
            'name' => 'Clothing & Fashion',
            'namefr'=>'Vêtements & Mode',
            ],
            [
            'id' => '3',
            'name' => 'Computers & Tablets',
            'namefr'=>'Ordinateurs et tablettes',
            ],
            [
            'id' => '4',
            'name' => 'PC Components',
            'namefr'=>'Composants PC',
            ]
            
                ]);
    }
}
