<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->collation('utf8_unicode_ci');
            $table->string('namefr', 500)->collation('utf8_unicode_ci');
            $table->string('descfr',1500)->collation('utf8_unicode_ci');
            $table->string('desc',1500)->collation('utf8_unicode_ci');
            $table->string('asin',100);
            $table->decimal('price',10,2);
            $table->integer('quantity')->unsigned();
            $table->timestamp('created_at')->useCurrent();
            $table->bigInteger( 'idcat' )->unsigned();
            $table->foreign('idcat')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Products');
    }
}
