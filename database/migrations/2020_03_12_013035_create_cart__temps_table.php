<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart__temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string( 'userid' );
            $table->bigInteger( 'idprod' )->unsigned();
            $table->foreign('idprod')->references('id')->on('products')->onDelete('cascade');
            $table->integer('qty')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart__temps');
    }
}
