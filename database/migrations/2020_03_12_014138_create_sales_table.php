<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger( 'iduser' )->unsigned();
            $table->foreign('iduser')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger( 'idprod' )->unsigned();
            $table->foreign('idprod')->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('qty')->unsigned();
            $table->bigInteger( 'discount' )->unsigned();
            $table->decimal('subtotal', 10, 2);
            $table->timestamp('saledate')->useCurrent();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
