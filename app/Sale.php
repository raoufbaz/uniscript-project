<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function Product()
    {
        return $this->hasOne('App\Product','id','idprod');
    }
}
