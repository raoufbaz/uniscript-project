<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category as Category;
use Illuminate\Support\Facades\DB;
use App\Product as Product;
use App\Discount as Discount;
use App\Comment as Comment;
use App\Sale as Sale;
use App\Rating as Rating;
use App\User as User;
use App\Cart;
use App\Wishlist;
use App\FeaturedProduct as FeaturedProduct;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
	{
        $cat=Category::all();
        $prod=FeaturedProduct::all();
		return view('index',["Categories"=>$cat])->with('Products',$prod);
	}


    public function cart()
    {
        return view('cart');
    }

}
