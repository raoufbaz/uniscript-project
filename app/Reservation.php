<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function Product()
    {
        return $this->hasOne('App\Product','id','idprod');
    }
}
