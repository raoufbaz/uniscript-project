<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Productvedette(){
        return $this->belongsTo('App\Productvedette','id','id');
    }
    public function Sale(){
        return $this->belongsTo('App\Sale','idprod','id');
    }
}
